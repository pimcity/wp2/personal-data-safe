import { AUTH_START, AUTH_SUCCESS, AUTH_FAIL, AUTH_LOGOUT } from '../actions/actionTypes'

const innitialState = {
    userId: null,
    username: null,
    error: null,
    loading: false,
}

const reducer = (state = innitialState, action) => {
    switch (action.type) {
        case AUTH_START:
            return {
                ...state,
                error: null,
                loading: true,
            }

        case AUTH_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                userId: action.payload.userId,
                username: action.payload.username,
            }

        case AUTH_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            }

        case AUTH_LOGOUT:
            return {
                userId: null,
                username: null,
                error: null,
                loading: false,
            }

        default:
            return state
    }
}

export default reducer
