// Packages
import axiosInstance from '../../axios/authAxios'
import jwt_decode from 'jwt-decode'

// Action types
import { AUTH_FAIL, AUTH_START, AUTH_SUCCESS, AUTH_LOGOUT } from './actionTypes'

// Starting User Authentication Procedure
export const authStart = () => {
    return {
        type: AUTH_START,
    }
}

// User Authentication was successful
export const authSuccess = (userId, username) => {
    return {
        type: AUTH_SUCCESS,
        payload: {
            userId: userId,
            username: username,
        },
    }
}

// User Authentication has failed
export const authFail = (error) => {
    return {
        type: AUTH_FAIL,
        error: error,
    }
}

// User Authentication logic
export const auth = (username, password) => {
    return (dispatch) => {
        // Starting the authentication
        dispatch(authStart())

        // Performing request
        const authData = JSON.stringify({
            username: username,
            password: password,
        })

        axiosInstance
            .post(`token/`, authData)
            .then((response) => {
                // Saving the tokens in local storage
                localStorage.setItem('pds_access_token', response.data.access)
                localStorage.setItem('pds_refresh_token', response.data.refresh)

                // Saving tokens expiration dates in local storage
                const accessExpirationDate = new Date(new Date().getTime() + response.data.access_lifetime * 1000)
                const refreshExpirationDate = new Date(new Date().getTime() + response.data.refresh_lifetime * 1000)
                localStorage.setItem('pds_access_exp_date', accessExpirationDate)
                localStorage.setItem('pds_refresh_exp_date', refreshExpirationDate)

                // Setting the axios instance default authorization with the access token
                axiosInstance.defaults.headers['Authorization'] = 'JWT ' + localStorage.getItem('pds_access_token')

                // Dispatching the successful action
                dispatch(authCheckTimeout(response.data.refresh_lifetime))
                dispatch(authSuccess(response.data.user_id, response.data.username))
            })
            .catch((error) => {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data)
                    console.log(error.response.status)
                    console.log(error.response.headers)
                    if (error.response.data.detail){
                        const errorMessage = error.response.status + " - " + error.response.data.detail
                        dispatch(authFail(errorMessage))
                    }
                } else if (error.request) {
                    // The request was made but no response was received
                    dispatch(authFail('No response from server, check your connection and try again..'))
                } else {
                    // Something happened in setting up the request that triggered an Error
                    dispatch(
                        authFail(
                            'Request was not properly set, please check internet connection and browser functionalities.'
                        )
                    )
                }

                // if (!error.response) {
                //     dispatch(authFail("An error occurred, check your connection and try again.."))
                // }
                // dispatch(authFail(error.response.data.detail))
            })
    }
}

// User Logout logic
export const logout = () => {
    // Blacklisting the token if present
    if (localStorage.getItem('pds_refresh_token') !== null) {
        axiosInstance.post(`users/logout/`, { refresh_token: localStorage.getItem('refresh_token') })
    }

    // Unsetting the tokens that are saved in local storage
    localStorage.removeItem('pds_access_token')
    localStorage.removeItem('pds_refresh_token')
    localStorage.removeItem('pds_access_exp_date')
    localStorage.removeItem('pds_refresh_exp_date')

    // Resetting state
    return {
        type: AUTH_LOGOUT,
    }
}

// Auto log out user when refresh token expires
export const authCheckTimeout = (expirationTime) => {
    return (dispatch) => {
        setTimeout(() => {
            dispatch(authCheckState())
        }, expirationTime * 1000)
    }
}

// Check state of authentication
export const authCheckState = () => {
    return (dispatch) => {
        // Get data from localStorage
        const access_token = localStorage.getItem('pds_access_token')
        const access_exp_date = localStorage.getItem('pds_access_exp_date')
        const refresh_token = localStorage.getItem('pds_refresh_token')
        const refresh_exp_date = localStorage.getItem('pds_refresh_exp_date')

        // Check if access token exists
        if (access_token && access_exp_date) {
            // Check if access token is still valid
            const accessExpirationDate = new Date(access_exp_date)
            if (accessExpirationDate > new Date()) {
                // Get useful info from token
                const decoded_jwt = jwt_decode(access_token)
                return dispatch(authSuccess(decoded_jwt.user_id, decoded_jwt.username))
            }
        }

        if (refresh_token && refresh_exp_date) {
            // Check if refresh token is still valid
            const refreshExpirationDate = new Date(refresh_exp_date)
            if (refreshExpirationDate > new Date()) {
                // Refresh token is valid, we use it to refresh the access token
                axiosInstance
                    .post(`token/refresh/`, { refresh: refresh_token })
                    .then((response) => {
                        // Update the local storage variables
                        const accessExpirationDate = new Date(
                            new Date().getTime() + response.data.access_lifetime * 1000
                        )
                        const access_token = response.data.access
                        localStorage.setItem('pds_access_exp_date', accessExpirationDate)
                        localStorage.setItem('pds_access_token', access_token)
                        axiosInstance.defaults.headers['Authorization'] = 'JWT ' + localStorage.getItem('pds_access_token')

                        const decoded_jwt = jwt_decode(access_token)
                        return dispatch(authSuccess(decoded_jwt.user_id, decoded_jwt.username))
                    })
                    .catch((error) => {
                        return dispatch(logout())
                    })
            }
        }

        return dispatch(logout())
    }
}
