import axios from 'axios'

const baseUrl = 'https://easypims.pimcity-h2020.eu/pds/api/'

const axiosInstance = axios.create({
    baseURL: baseUrl,
    timeout: 5000,
    headers: {
        Authorization: localStorage.getItem('pds_access_token')
        ? 'JWT ' + localStorage.getItem('pds_access_token')
        : null,
        'Content-Type': 'application/json',
        accept: 'application/json'
    }
})

export default axiosInstance
