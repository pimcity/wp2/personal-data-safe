// Packages
import React, {useEffect} from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import {useDispatch} from 'react-redux'

// Components
import PersonalDataPage from './components/Pages/PersonalDataPage/PersonalDataPage'
import AuthPage from './components/Pages/AuthPage/AuthPage'
import LogoutPage from './components/Pages/LogoutPage'

// Redux
import {authCheckState} from './store/actions/index'

// CSS
import './App.css'

function App() {

    // Redux 
    const dispatch = useDispatch()

    // Component mount
    useEffect(() => {
        dispatch(authCheckState())
    }, [dispatch])

    return (
        <React.Fragment>
            <Router basename="/pds">
                <Switch>
                    <Route path="/login" component={AuthPage} />
                    <Route path="/logout" component={LogoutPage} />
                    <Route path="/" component={PersonalDataPage} />
                </Switch>
            </Router>
        </React.Fragment>
    )
}

export default App
