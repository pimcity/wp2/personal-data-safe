import React from 'react'
import './SidedrawerButton.css'

const SidedrawerButton = (props) => {
    return (
        <div className={'sidebar-button'} onClick={props.clicked}>
            <div className="line1"></div>
            <div className="line2"></div>
            <div className="line3"></div>
        </div>
    )
}

export default SidedrawerButton
