import React from 'react'
import './BlackscreenWithClick.css'

function BlackscreenWithClick(props) {
    return (
        <div className="blackscreen-with-click" onClick={props.clickFunction}></div>
    )
}

export default BlackscreenWithClick