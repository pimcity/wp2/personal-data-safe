import React from 'react'
import { BsXCircle } from 'react-icons/bs'
import './CustomModal.css'

export default function CustomModal(props) {

    let modalClass = 'big-modal-content'
    if(props.type && props.type === 'small') {
        modalClass = 'small-modal-content'
    }
    
    return (
        <div className="custom-modal">
            <div className="custom-modal-backscreen" onClick={props.closeModal}></div>
            <div className={modalClass}>
                <button className="btn btn-danger close-button" onClick={props.closeModal}>
                    <i className="d-flex h-100 align-items-center">
                        <BsXCircle />
                    </i>
                </button>
                {props.children}
            </div>
        </div>
    )
}
