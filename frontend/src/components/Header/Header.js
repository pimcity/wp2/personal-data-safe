// Packages
import React from 'react'

// Local files
import SidedrawerButton from '../UI/SidedrawerButton/SidedrawerButton'

// Bootstrap
import { Navbar } from 'react-bootstrap'
import { UnlockFill } from 'react-bootstrap-icons'
import './Header.css'
import 'bootstrap/dist/css/bootstrap.min.css'

export default function Header( props ) {
    return (
        <React.Fragment>
            <Navbar expand="lg" className="d-flex justify-content-between align-items-center px-sm-5 pds-navbar">
                <SidedrawerButton clicked={props.toggleSideDrawer} />
                <Navbar.Brand className="p-0">
                    <UnlockFill width="20" height="20" className="mb-2" /> Personal Data Safe
                </Navbar.Brand>
            </Navbar>
        </React.Fragment>
    )
}
