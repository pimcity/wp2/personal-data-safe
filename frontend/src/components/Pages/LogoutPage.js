// Packages
import React, {useEffect} from 'react'
import { useDispatch } from 'react-redux'
import { Redirect } from 'react-router'


// Custom Files
import {logout} from '../../store/actions/index'

export default function LogoutPage() {

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(logout())
    })

    return (
        <Redirect to="/" />
    )
}
