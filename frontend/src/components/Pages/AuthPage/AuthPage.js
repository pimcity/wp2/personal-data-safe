// Packages
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Redirect } from 'react-router'
import { useHistory } from 'react-router-dom'

// Redux actions
import { auth } from '../../../store/actions/index'

// Custom functions
import validateAuthForm from '../../../utility/validateAuthForm'
import './AuthPage.css'

export default function AuthPage(props) {
    // Hooks setup
    const dispatch = useDispatch()
    const username = useSelector((state) => state.auth.username)
    const loginError = useSelector((state) => state.auth.error)
    let history = useHistory()

    // Local state management
    const [form, setForm] = useState({
        username: '',
        password: '',
    })

    const [formErrors, setFormErrors] = useState(validateAuthForm(form))

    // Custom functions
    const handleFormChange = (event) => {
        const { name, value } = event.target

        const updatedForm = { ...form }

        updatedForm[name] = value

        setForm(updatedForm)
        setFormErrors(validateAuthForm(updatedForm))
    }

    // Form submission
    const formSubmit = (event) => {
        // Prevent reloading of page
        event.preventDefault()

        // Checking if the form is valid (object formErrors must be empty)
        if (Object.keys(formErrors).length === 0) {
            dispatch(auth(form.username, form.password))
        }
    }

    // If the field auth.username is present then the user is logged in
    if (username) {
        if (props.location.state) {
            return <Redirect to={props.location.state} />
        }

        return <Redirect to={'/'} />
    }

    // State dependend components
    const usernameAlert = formErrors['username'] ? (
        <label className="mx-2 text-danger ">
            <small>{formErrors['username']}</small>
        </label>
    ) : null

    const passwordAlert = formErrors['password'] ? (
        <label className="mx-2 text-danger ">
            <small>{formErrors['password']}</small>
        </label>
    ) : null

    const loginErrorAlert = loginError ? (
        <div class="alert alert-danger" role="alert">
            {loginError}
        </div>
    ) : null

    return (
        <div className="auth-page bg-light p-5">
            <div className="row justify-content-center">
                <div className="col-12 col-md-7 col-lg-5 col-xl-4 rounded p-3 shadow-lg login">
                    <h1 className="text-center mb-3">Personal Data Safe</h1>
                    <h3 className="text-center mb-3">Login</h3>
                    {loginErrorAlert}
                    <form onSubmit={formSubmit}>
                        <div className="form-group">
                            <label>
                                <small>Username</small>
                            </label>
                            <input
                                onChange={handleFormChange}
                                name="username"
                                className="form-control"
                                placeholder="Enter username"
                                value={form.username}
                            />
                            {usernameAlert}
                        </div>
                        <div className="form-group">
                            <label>
                                <small>Password</small>
                            </label>
                            <input
                                onChange={handleFormChange}
                                type="password"
                                name="password"
                                className="form-control"
                                placeholder="Password"
                                value={form.password}
                            />
                            {passwordAlert}
                        </div>
                        <div className="d-flex justify-content-center">
                            <button
                                type="submit"
                                className="btn btn-success"
                                disabled={Object.keys(formErrors).length !== 0}
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
