// Packages
import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router'

// Local Files
import Header from '../../Header/Header'
import Footer from '../../Footer/Footer'
import Sidedrawer from '../../Sidedrawer/Sidedrawer'
import Sidebar from '../../Sidebar/Sidebar'
import PersonalSafeHome from '../../DataSafeComponents/PersonalSafeHome/PersonalSafeHome'
import PersonalSafeUserInsertion from '../../DataSafeComponents/PersonalSafeUserInsertion/PersonalSafeUserInsertion'
import PersonalSafeView from '../../DataSafeComponents/PersonalSafeView/PersonalSafeView'
import authAxios from '../../../axios/authAxios'
import './PersonalDataPage.css'

export default function PersonalDataPage() {
    const [macroGroups, setMacroGroups] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [isSidedrawerOpen, setIsSidedrawerOpen] = useState(false)
    const username = useSelector((state) => state.auth.username)

    useEffect(() => {
        if (username) {
            setIsLoading(true)

            authAxios
                .get(`v1/utility/macro-groups`)
                .then((res) => {
                    let data = [...res.data]
                    setMacroGroups(data)
                    setIsLoading(false)
                })
                .catch((err) => {
                    setIsLoading(false)
                })
        }
    }, [])

    let pageContent = null
    let componentRouter = null

    if (isLoading) {
        pageContent = <h3 className="data-safe-component text-center">Loading...</h3>
    } else {
        if (!macroGroups.length) {
            pageContent = <h3 className="data-safe-component text-center">Could not load data...</h3>
        } else {
            componentRouter = (
                <Switch>
                    <Route exact path="/" component={() => <PersonalSafeHome groups={macroGroups} />} />
                    {macroGroups.map((group) => {
                        const linkName = '/' + group['group-name']

                        if (group['user-insertion']) {
                            return (
                                <Route
                                    exact
                                    path={linkName}
                                    component={() => (
                                        <PersonalSafeUserInsertion
                                            group={group['group-name']}
                                            insertions={group['types']}
                                        />
                                    )}
                                    key={group['group-name']}
                                />
                            )
                        }

                        return (
                            <Route
                                exact
                                path={linkName}
                                component={() => (
                                    <PersonalSafeView
                                        group={group['group-name']}
                                        insertions={group['types']}
                                        visualizationHint={group['visualization-hint']}
                                    />
                                )}
                                key={group['group-name']}
                            />
                        )
                    })}

                    <Route path="/" render={() => <Redirect to="/" />} />
                </Switch>
            )

            pageContent = (
                <div className="d-flex flex-row justify-content-between">
                    <div className="sidebar-column col-lg-2 p-0">
                        <Sidebar groups={macroGroups} />
                    </div>
                    <div className="col data-safe-component bg-light">{componentRouter}</div>
                </div>
            )
        }
    }

    if (!username) {
        return <Redirect to={{ pathname: '/login', state: window.location.pathname }} />
    }

    const toggleSideDrawer = () => {
        const currentState = isSidedrawerOpen
        setIsSidedrawerOpen(!currentState)
    }

    return (
        <div>
            <Header toggleSideDrawer={toggleSideDrawer} />
            <Sidedrawer toggleSideDrawer={toggleSideDrawer} isSidedrawerOpen={isSidedrawerOpen} groups={macroGroups} />
            {pageContent}
            <Footer />
        </div>
    )
}
