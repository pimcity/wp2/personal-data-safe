import React from 'react'
import { Container } from 'react-bootstrap'
import './Footer.css'
import 'bootstrap/dist/css/bootstrap.min.css'

const footers = [
    {
        title: 'Company',
        description: ['Team', 'History', 'Contact us', 'Locations'],
    },
    {
        title: 'Features',
        description: ['Cool stuff', 'Random feature', 'Team feature', 'Developer stuff', 'Another one'],
    },
    {
        title: 'Resources',
        description: ['Resource', 'Resource name', 'Another resource', 'Final resource'],
    },
    {
        title: 'Legal',
        description: ['Privacy policy', 'Terms of use'],
    },
]

const footerContent = (
    <div className="pt-3 footer">
        <Container>
            <div className="row">
                {footers.map((footer) => (
                    <div className="col h4 text-center" key={footer.title}>
                        {footer.title}
                    </div>
                ))}
            </div>
            <div className="row">
                {footers.map((footer) => (
                    <div className="col" key={footer.title}>
                        <ul className="list-group list-group-flush">
                            {footer.description.map((item) => (
                                <li className="list-group-item text-center" key={item}>
                                    {item}
                                </li>
                            ))}
                        </ul>
                    </div>
                ))}
            </div>
        </Container>
    </div>
)

export default function Footer() {
    return (
        <div className="border-top d-flex justify-content-center align-items-center footer bg-light">
           <p className="text-muted font-italic m-0"><small>Copyright © 2021 Politecnico di Torino. All Rights Reserved</small></p>
        </div>
    )
}
