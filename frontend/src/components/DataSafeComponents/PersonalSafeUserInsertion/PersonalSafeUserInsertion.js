import React, { useState, useEffect } from 'react'

import EditableRow from './EditableRow'
import authAxios from '../../../axios/authAxios'
import { insertionsFormatter, groupNameFormatter } from '../../../utility/formatters'
import { isEmptyDict } from '../../../utility/utility'

function PersonalSafeUserInsertion(props) {
    const [isLoading, setIsLoading] = useState(false)
    const [form, setForm] = useState({})

    useEffect(() => {
        setIsLoading(true)

        let api_point = 'v1/personal-data/'
        let params = {
            category: props.group,
        }

        authAxios
            .get(api_point, { params: params })
            .then((res) => {
                // Unpack and store data in the temporary formatted form
                let datas = [...res.data.data]
                let tempForm = insertionsFormatter(props.insertions)

                datas.forEach((data) => {
                    const dataName = Object.keys(data['value'])[0]
                    tempForm[dataName]['id'] = data['id']
                    tempForm[dataName]['value'] = data['value'][dataName]
                    tempForm[dataName]['original'] = data['value'][dataName]
                })

                setForm(tempForm)
                setIsLoading(false)
            })
            .catch((err) => {
                setIsLoading(false)
            })
    }, [props.group, props.insertions])

    // Function to handle changes in the forms
    const handleFormChange = (event) => {
        const { name, value } = event.target

        let updatedForm = { ...form }
        let updatedValue = { ...updatedForm[name] }

        updatedForm[name] = updatedValue
        updatedValue['value'] = value
        updatedForm[name] = updatedValue

        setForm(updatedForm)
    }

    // Function to reset entry to original value
    const revertChange = (data) => {
        let updatedForm = { ...form }
        let updatedValue = { ...updatedForm[data] }

        updatedForm[data] = updatedValue
        updatedValue['value'] = form[data]['original']
        updatedForm[data] = updatedValue

        setForm(updatedForm)
    }

    // Function to handle user insertion/modification of personal data
    const modifyEntry = (data) => {
        // Check if data is new or to be modified
        if (form[data]['id']) {
            let api_point = 'v1/personal-data/' + form[data]['id'] + '/'

            const api_data = {
                'group-name': props.group,
                'value-name': data,
                'value-new': form[data]['value'],
            }

            authAxios
                .put(api_point, api_data)
                .then((res) => {
                    // If PUT request is successfull then update the form with the new value
                    let updatedForm = { ...form }
                    let updatedValue = { ...updatedForm[data] }

                    updatedForm[data] = updatedValue
                    updatedValue['original'] = res.data.value[data]
                    updatedForm[data] = updatedValue

                    setForm(updatedForm)
                })
                .catch((err) => {
                    console.log(err)
                })
        } else {
            let api_point = 'v1/personal-data/'

            const api_data = {
                'group-name': props.group,
                type: data,
                metadata: data,
                value: form[data]['value'],
            }

            authAxios
                .post(api_point, api_data)
                .then((res) => {
                    // If PUT request is successfull then update the form with the new value
                    let updatedForm = { ...form }
                    let updatedValue = { ...updatedForm[data] }

                    updatedForm[data] = updatedValue
                    updatedValue['original'] = res.data.value[data]
                    updatedForm[data] = updatedValue
                    updatedForm[data]['id'] = res.data.id

                    setForm(updatedForm)
                })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    // Defining the page content
    let pageContent = null
    if (isLoading) {
        pageContent = <h3 className="text-center">Loading...</h3>
    } else {
        if (isEmptyDict(form)) {
            pageContent = <h3 className="text-center">Could not load data...</h3>
        } else {
            pageContent = Object.keys(form).map((data) => {
                return (
                    <EditableRow
                        key={data}
                        data={data}
                        value={form[data]['value']}
                        data_type={form[data]['data-type']}
                        isModified={form[data]['value'] !== form[data]['original']}
                        handleFormChange={handleFormChange}
                        modifyEntry={() => modifyEntry(data)}
                        revertChange={() => revertChange(data)}
                    />
                )
            })
        }
    }

    return (
        <div className="w-100 p-3">
            <h2 className="text-left ml-3">{groupNameFormatter(props.group)}</h2>
            <hr className="mb-3" />
            <div className="d-flex flex-column m-3 justify-content-around">{pageContent}</div>
        </div>
    )
}

export default PersonalSafeUserInsertion
