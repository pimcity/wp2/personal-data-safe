import React from 'react'

import EditButtons from './EditButtons'
import { groupNameFormatter } from '../../../utility/formatters'

function EditableRow(props) {

    const onEnterSubmit = (event) => {
        if (event.key === 'Enter') {
            props.modifyEntry()
        }
    }

    return (
        <div className="col-12 my-2 px-0 px-lg-3">
            <div className="row">
                <h6 className="col-3 px-0 px-lg-3">{groupNameFormatter(props.data)}</h6>
                <div className="col-9">
                    <div className="row justify-content-center">
                        <input
                            onChange={props.handleFormChange}
                            onKeyPress={onEnterSubmit}
                            name={props.data}
                            className="col-6 col-md-8"
                            value={props.value}
                            type={props.data_type}
                        />
                        <div className="col-4 col-md-2">
                            <EditButtons
                                modifyEntry={props.modifyEntry}
                                revertChange={props.revertChange}
                                isModified={props.isModified}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div>
    )
}

export default EditableRow
