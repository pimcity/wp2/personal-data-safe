import React from 'react'

import { BsBrush } from 'react-icons/bs'
import {MdSettingsBackupRestore} from 'react-icons/md'

function EditButtons(props) {
    return (
        <div className="row justify-content-center align-content-center">
            <div className="col-5">
                <button className="btn btn-sm btn-success" onClick={props.modifyEntry} disabled={!props.isModified}>
                    <BsBrush />
                </button>
            </div>
            <div className="col-5">
                <button className="btn btn-sm btn-danger" onClick={props.revertChange} disabled={!props.isModified}>
                    <MdSettingsBackupRestore />
                </button>
            </div>
        </div>
    )
}

export default EditButtons
