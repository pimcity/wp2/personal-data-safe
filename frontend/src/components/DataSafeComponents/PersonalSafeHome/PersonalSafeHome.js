import React, { useState } from 'react'
import { Link } from 'react-router-dom'

// Local
import authAxios from '../../../axios/authAxios'
import Charts from './Charts/Charts'
import UploadZipModal from './UploadZipModal/UploadZipModal'
import { groupNameFormatter, base64ToBlob } from '../../../utility/formatters'

//Css
import './PersonalSafeHome.css'

function PersonalSafeHome(props) {
    // Set up state variables
    const [isUploadModalOpen, setIsUploadModalOpen] = useState(false)

    // Function to send an API GET request to extract data and download it as a .zip file
    const extractData = (groupNames) => {
        // Create a list of query names from the passed names, otherwise leave empty
        let queryNames = []

        if (groupNames !== null) {
            queryNames.push(groupNames)
        }

        // Set up parameters for api call
        let api_point = 'v1/personal-data/zip-file/'
        let params = {
            'group-names': queryNames,
        }

        authAxios
            .get(api_point, { params: params })
            .then((res) => {
                // Unpack and store data in the temporary formatted form
		console.log("here")
                var a = document.createElement('a')
                if (window.URL && window.Blob && window.atob) {
                    // Do it the HTML5 compliant way
                    var blob = base64ToBlob(res.data.download.data, 'application/zip')
                    var url = window.URL.createObjectURL(blob)
                    a.href = url
                    a.download = 'archive.zip'
                    a.click()
                    window.URL.revokeObjectURL(url)
                } else {
                    console.log('error')
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    // Set up of the first row of actions which are fixed for all users
    const firstRowActions = (
        <div className="d-flex flex-wrap bd-highlight mb-3 justify-content-around">
            {/* "Reset Password" card */}
            <div className="col-10 col-lg-4 mb-4">
                <div className="card h-100">
                    <div className="card-body d-flex flex-column justify-content-between align-items-center shadow">
                        <h6 className="card-title">Reset your password</h6>
                        <a className="btn btn-secondary" href="{% url 'password_reset' %}">
                            Reset
                        </a>
                    </div>
                </div>
            </div>

            {/* "Zip Upload" card */}
            <div className="col-10 col-lg-4 mb-4">
                <div className="card">
                    <div className="card-body d-flex flex-column justify-content-between align-items-center shadow">
                        <h6 className="card-title">Upload zip file</h6>
                        <div className="zip row mb-0 justify-content-around align-content-between">
                            <button
                                className="btn btn-secondary"
                                type="button"
                                id="upload_zip"
                                onClick={() => setIsUploadModalOpen(!isUploadModalOpen)}
                            >
                                Upload
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {/* "Extract Data" card */}
            <div className="col-10 col-lg-4 mb-4">
                <div className="card h-100">
                    <div className="card-body d-flex flex-column justify-content-between align-items-center shadow">
                        <h6 className="card-title">Extract your data</h6>
                        <button onClick={() => extractData(null)} className="btn btn-secondary">
                            Extract
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )

    // Set up of the group cards. Group cards can vary and be different therefore we use the info fetched from API
    let groupCards = props.groups.map((group) => {
        const linkName = '/' + group['group-name']

        return (
            <div className="col-10 col-lg-4 mb-4" key={group['group-name']}>
                <div className="card h-100">
                    <div className="card-body d-flex flex-column justify-content-between align-items-center shadow">
                        <h6 className="card-title">{groupNameFormatter(group['group-name'])}</h6>
                        <div className="card-buttons flex d-flex justify-content-around align-items-center">
                            <button onClick={() => extractData(group['group-name'])} className="btn btn-secondary">
                                Extract
                            </button>
                            <Link to={linkName} className="btn btn-secondary">
                                View
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    })

    return (
        <div className="personal-safe-home">
            {isUploadModalOpen ? (
                <UploadZipModal
                    closeModal={() => setIsUploadModalOpen(false)}
                    groups={props.groups.map((group) => group['group-name'])}
                />
            ) : null}
            <div className="p-3">
                <h2 className="text-center">PDS Home</h2>
                <hr />
                {firstRowActions}
                <hr />
                <h3 className="text-center mb-4">These are your data groups</h3>
                <div className="d-flex flex-wrap bd-highlight mb-3 justify-content-around">{groupCards}</div>
                <div className="d-flex flex-wrap bd-highlight justify-content-around align-items-center mb-4 px-3 charts">
                    <Charts />
                </div>
            </div>
        </div>
    )
}

export default PersonalSafeHome
