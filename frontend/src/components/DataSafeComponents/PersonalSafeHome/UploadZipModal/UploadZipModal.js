import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router'
import authAxios from '../../../../axios/authAxios'
import CustomModal from '../../../UI/CustomModal/CustomModal'

// Styles
import { BsXCircle } from 'react-icons/bs'
import './UploadZipModal.css'

export default function UploadZipModal(props) {
    // Setting up component state
    const [formSettings, setFormSettings] = useState([])
    const [file, setFile] = useState('')
    const [filename, setFilename] = useState('Choose File')
    const [errorMessage, setErrorMessage] = useState('')
    const [isError, setIsError] = useState(false)
    const [isSuccess, setIsSuccess] = useState(false)

    useEffect(() => {
        let tempFormSettings = []

        props.groups.forEach((group) => {
            tempFormSettings.push({
                name: group,
                chosen: false,
            })
        })

        setFormSettings(tempFormSettings)
    }, [])

    // This function handles changes in the checkboxes values
    const handleToggle = (name) => {
        let tempFormSettings = formSettings

        tempFormSettings.forEach((element) => {
            if (element.name === name) {
                element.chosen = !element.chosen
            }
        })

        setFormSettings(tempFormSettings)
    }

    // Displaying the checkboxes based on the settings
    const chackboxes = formSettings.map((setting) => (
        <div
            className="col-12 col-sm-6 d-flex justify-content-center justify-content-sm-start align-items-center"
            key={setting.name}
        >
            <input
                className="mr-3"
                type="checkbox"
                value={setting.chosen}
                id="defaultCheck1"
                onChange={() => handleToggle(setting.name)}
            />
            <p className="form-check-label">{setting.name}</p>
        </div>
    ))

    // This function deals with new changes in the upload files
    const fileChange = (event) => {
        setFile(event.target.files[0])
        setFilename(event.target.files[0].name)
    }

    // This function performs the API POST request to upload the zip file
    const submitForm = (event) => {
        event.preventDefault()
        setIsError(false)

        if (file === '') {
            setIsError(true)
            setErrorMessage('Missing zip file')
            return null
        }

        const formData = new FormData()
        formData.append('zip', file)

        let api_point = 'v1/personal-data/zip-file/'
        let group_names = []

        formSettings.forEach((element) => {
            if (element.chosen) {
                group_names.push(element.name)
            }
        })

        if (group_names.length === 0) {
            setIsError(true)
            setErrorMessage('Must specify at least one field')

            return null
        }

        formData.append('group_names', group_names)

        authAxios
            .post(api_point, formData, {timeout: 200000})
            .then((res) => {
                setIsSuccess(true)
            })
            .catch((err) => {
                setIsError(true)

                try{
                    if (err.response.data['error_message']) {
                        setErrorMessage(err.response.data['error_message'])
                    }
                } catch {
                    console.log(err)
                    setErrorMessage('An error occurred')
                }
            })
    }

    const alert = (
        <div className="alert alert-danger" role="alert">
            {errorMessage}
        </div>
    )

    if (isSuccess) {
        return <Redirect to="/" />
    }

    return (
        <CustomModal closeModal={props.closeModal}>
                <div className="zip-modal-form">
                    <h1 className="text-center">Uplaod zip</h1>
                    <hr />
                    {isError ? alert : null}
                    <h4>Select which categories to add:</h4>
                    <div className="zip-modal-choices row">{chackboxes}</div>
                </div>
                <form className="zip row mb-0 justify-content-around align-content-between" onSubmit={submitForm}>
                    <div className="custom-file">
                        <input
                            className="custom-file-input"
                            type="file"
                            accept=".zip"
                            onChange={fileChange}
                            id="customFile"
                        />
                        <label className="custom-file-label" htmlFor="customFile">
                            {filename}
                        </label>
                    </div>
                    <input type="submit" value="Submit" className="btn btn-success mt-2" />
                </form>
        </CustomModal>
    )
}
