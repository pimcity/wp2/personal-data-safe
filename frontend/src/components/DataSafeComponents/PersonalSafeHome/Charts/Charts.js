// Packages imports
import React, { useEffect, useState } from 'react'
import { Bar, Doughnut, Line } from 'react-chartjs-2'

// Local files
import authAxios from '../../../../axios/authAxios'
import { indexColor } from '../../../../utility/colorList'

let options = {
    legend: {
        position: 'bottom',
    },
    responsive: true,
    maintainAspectRatio: false,
}

let optionScales = {
    legend: {
        display: false,
    },
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }],
        xAxes: [
            {
                ticks: {
                    callback: function (tick) {
                        var characterLimit = 8
                        if (tick.length >= characterLimit) {
                            return (
                                tick
                                    .slice(0, tick.length)
                                    .substring(0, characterLimit - 1)
                                    .trim() + '...'
                            )
                        }
                        return tick
                    },
                },
            },
        ],
    },
}

export default function Charts() {
    const [barChartData, setBarChartData] = useState({})
    const [doughChartData, setDoughChartData] = useState({})
    const [datesChartData, setDatesChartData] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [isError, setIsError] = useState(false)

    useEffect(() => {
        setIsLoading(true)

        let api_point = 'v1/utility/graph-data'

        authAxios
            .get(api_point)
            .then((res) => {
                try {
                    // Unpack and store data in the temporary formatted form
                    let datas = [...res.data['macro-groups']]
                    let labels = []
                    let data = []
                    let backgroundColor = []
                    let borderColor = []

                    let colorIndex = 0

                    datas.forEach((group) => {
                        labels.push(group['group_name'])
                        data.push(group['total'])

                        let randomColor = `rgba${indexColor(colorIndex, 0.6)}`
                        colorIndex += 1

                        backgroundColor.push(randomColor)

                        randomColor = randomColor.replace('0.2)', '1)')
                        borderColor.push(randomColor)
                    })

                    setBarChartData({
                        labels: labels,
                        datasets: [
                            {
                                label: 'Personal Data',
                                data: data,
                                backgroundColor: backgroundColor,
                                borderColor: borderColor,
                                borderWidth: 4,
                            },
                        ],
                    })

                    setDoughChartData({
                        labels: labels,
                        datasets: [
                            {
                                label: 'Personal Data',
                                data: data,
                                backgroundColor: backgroundColor,
                            },
                        ],
                    })

                    datas = [...res.data['data-days']]
                    data = []
                    labels = []

                    datas.forEach((date) => {
                        labels.push(date['day'].substring(0, 10))

                        if (data.length > 0) {
                            let prev_count = data[data.length - 1]
                            data.push(date['total'] + prev_count)
                        } else {
                            data.push(date['total'])
                        }
                    })

                    setDatesChartData({
                        labels: labels,
                        datasets: [
                            {
                                label: 'Personal Data',
                                data: data,
                                steppedLine: true,
                            },
                        ],
                    })
                } catch (error) {
                    setIsLoading(false)
                    setIsError(true)
                }

                setIsLoading(false)
            })
            .catch((err) => {
                setIsLoading(false)
                setIsError(true)
            })
    }, [])

    let charts = null

    if (isLoading) {
        charts = <p>Loading...</p>
    } else if (isError) {
        charts = <p>An error occurred... try refreshing the page</p>
    } else {
        charts = (
            <>
                <div className="card col-12 col-md-5 w-100 mb-4 shadow">
                    <div className="card-body">
                        <Bar
                            className="bar-chart"
                            data={barChartData}
                            height={3}
                            width={10}
                            options={{ ...options, ...optionScales }}
                        />
                    </div>
                </div>
                <div className="card col-12 col-md-5 w-100 mb-4 shadow">
                    <div className="card-body ">
                        <Doughnut className="bar-chart" data={doughChartData} height={4} width={10} options={options} />
                    </div>
                </div>
                <div className="card col-12 col-md-8 w-100 mb-4 shadow">
                    <div className="card-body">
                        <Line className="bar-chart" data={datesChartData} height={3} width={10} options={{...options, ...optionScales}} />
                    </div>
                </div>
            </>
        )
    }

    return charts
}
