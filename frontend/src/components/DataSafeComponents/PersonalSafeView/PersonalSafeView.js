import React, { useState, useEffect } from 'react'

import TableView from './TableView'
import MapModal from './MapModal'
import DeleteModal from './DeleteModal'
import PagesControls from './PagesControls'
import authAxios from '../../../axios/authAxios'
import { groupNameFormatter } from '../../../utility/formatters'
import { BsXCircle } from 'react-icons/bs'
import './PersonalSafeView.css'


function PersonalSafeView(props) {
    // Set up of react state
    const [isLoading, setIsLoading] = useState(false)
    const [dataList, setDataList] = useState({})
    const [dataListSettings, setDataListSettings] = useState({
        startInterval: 0,
        lengthInterval: 12,
        order: 'time',
        desc: false,
        currentPage: 1,
    })
    const [isMapModal, setIsMapModal] = useState(false)
    const [mapCoordinates, setMapCoordinates] = useState([])
    const [deleteModal, setDeleteModal] = useState(null)
    const [errorMessage, setErrorMessage] = useState(null)

    // This function performs the API request to get items from the backend
    const getPersonalData = () => {
        let api_point = 'v1/personal-data/'
        let params = {
            category: props.group,
            'filter-start': dataListSettings.startInterval,
            'filter-end': dataListSettings.startInterval + dataListSettings.lengthInterval,
            'order-by': dataListSettings.order,
            'order-type': dataListSettings.desc ? 'desc' : '',
        }

        authAxios
            .get(api_point, { params: params })
            .then((res) => {
                // Unpack and store data in the temporary formatted form
                let datas = [...res.data.data]
                let datasInfo = res.data['query-details']
                datasInfo.returnedObjects = datas.length

                setDataList({
                    data: datas,
                    dataInfo: datasInfo,
                })

                setIsLoading(false)
            })
            .catch((err) => {
                setErrorMessage('An error occurred')
                setIsLoading(false)
            })
    }

    // On component mount we fetch data from API
    useEffect(() => {
        setIsLoading(true)

        getPersonalData()
    }, [props.group, dataListSettings])

    // This function changes settings for the API fetch of data
    const changeFetchSettings = (start = null, length = null, order = null, orderType = null, currentPage = null) => {
        let tempParams = { ...dataListSettings }

        if (start) {
            tempParams.startInterval = start
        }

        if (length) {
            tempParams.lengthInterval = length
        }

        if (order) {
            tempParams.order = order
        }

        if (orderType != null) {
            tempParams.desc = orderType
            tempParams.currentPage = 1
        }

        if (currentPage != null) {
            tempParams.startInterval = (currentPage - 1) * dataListSettings.lengthInterval
            tempParams.currentPage = currentPage
        }

        setDataListSettings(tempParams)
    }

    // This function handles the input of total object displayed at once
    const changeTotObjDisplayed = (event) => {
        const { value } = event.target

        changeFetchSettings(null, value)
    }

    // This function open the map modal if the coordinates passed are ok
    const clickedMap = (coordinates) => {
        if (typeof coordinates !== 'undefined' && coordinates.length !== 0) {
            let isCoordinatesOk = true

            coordinates.forEach((point) => {
                if (point.lat == null || point.lng == null) {
                    isCoordinatesOk = false
                }
            })

            if (isCoordinatesOk) {
                setMapCoordinates(coordinates)
                setIsMapModal(true)
            }
        }
    }

    // This function loads all the coordinates on the currently displayed objects into the map modal
    const showAllOnMap = () => {
        let coordinates = []

        dataList.data.forEach((data) => {
            let categories = Object.keys(data.value)

            categories.forEach((category) => {
                coordinates.push({
                    lat: data.value[category]['latitude'],
                    lng: data.value[category]['longitude'],
                    category: category,
                })
            })
        })

        if (coordinates.length !== 0) {
            setMapCoordinates(coordinates)
            setIsMapModal(true)
        }
    }

    // This function closes the map modal
    const closeMapModal = () => {
        setMapCoordinates([])
        setIsMapModal(false)
    }

    // Delete Modal logic
    const openDeleteModal = (pk) => {
        setDeleteModal(pk)
        setErrorMessage(null)
    }

    const closeDeleteModal = () => {
        setDeleteModal(null)
    }
 
    // This function deletes one element with a DELETE request at the API and if successful
    // retrieves additional items as with component mount
    // We use the variable deleteModal to both open and close modal and to save the pk to be deleted
    const deleteData = () => {
        setErrorMessage(null)

        let api_point = 'v1/personal-data/' + deleteModal + '/'

        authAxios
            .delete(api_point)
            .then((res) => {
                getPersonalData()
                setDeleteModal(null)
            })
            .catch((err) => {
                setErrorMessage('An error occurred')
                setDeleteModal(null)
            })
    }

    console.log(deleteModal)


    let table = null
    if (isLoading) {
        table = <h3 className="text-center">Loading...</h3>
    } else if (!dataList.hasOwnProperty('data')) {
        table = <h3 className="text-center">Could not load data...</h3>
    } else if (dataList.data.length === 0) {
        table = <h3 className="text-center">No data...</h3>
    } else {
        table = (
            <>
                <div className="table">
                    <TableView
                        dataList={dataList.data}
                        insertions={props.insertions[0]}
                        visualizationHint={props.visualizationHint}
                        clickedMap={clickedMap}
                        showAllOnMap={showAllOnMap}
                        changeFetchSettings={changeFetchSettings}
                        openDeleteModal={openDeleteModal}
                        orderBy={dataListSettings.order}
                        orderType={dataListSettings.desc}
                    />
                </div>
                <div className="table-settings">
                    <PagesControls
                        startInterval={dataListSettings.startInterval}
                        lengthInterval={dataListSettings.lengthInterval}
                        currentPage={dataListSettings.currentPage}
                        returnedObjects={dataList.dataInfo.returnedObjects}
                        totalObjs={dataList.dataInfo['total-objects']}
                        changeTotObjDisplayed={changeTotObjDisplayed}
                        changeFetchSettings={changeFetchSettings}
                    />
                </div>
            </>
        )
    }

    const errorAlert = errorMessage ? (
        <div className="alert-message-custom alert alert-danger" role="alert">
            {errorMessage}
            <button className="btn btn-danger" onClick={() => setErrorMessage(null)}><BsXCircle/></button>
        </div>
    ) : null

    return (
        <div className="personal-safe-view w-100 p-3">
            {errorAlert}
            <div className="title">
                <h2 className="text-left ml-3">{groupNameFormatter(props.group)}</h2>
                <hr className="mb-3" />
            </div>
            {isMapModal ? <MapModal coordinates={mapCoordinates} closeModal={closeMapModal} /> : null}
            {deleteModal ? <DeleteModal closeModal={closeDeleteModal} deleteData={deleteData} /> : null}
            {table}
        </div>
    )
}

export default PersonalSafeView
