import React from 'react'
import CustomModal from '../../UI/CustomModal/CustomModal'
import { BsXCircle } from 'react-icons/bs'
import './DeleteModal.css'

export default function DeleteModal(props) {
    return (
        <CustomModal closeModal={props.closeModal} type="small">
            <div>
                <h3 className="text-center">Are you sure?</h3>
                <hr />
                <h5 className="text-center">This action cannot be undone</h5>
            </div>
            <div className="delete-data-buttons d-flex justify-content-around align-items-center mt-4">
                <button
                    className="btn btn-danger d-flex justify-content-center align-content-center p-0"
                    onClick={props.deleteData}
                >
                    Confirm
                </button>
                <button
                    className="btn btn-primary d-flex justify-content-center align-content-center p-0"
                    onClick={props.closeModal}
                >
                    Cancel
                </button>
            </div>
        </CustomModal>
    )
}
