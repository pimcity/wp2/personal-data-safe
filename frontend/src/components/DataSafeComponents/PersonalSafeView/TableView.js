import React from 'react'

import TableRow from './TableRow'
import { groupNameFormatter } from '../../../utility/formatters'

import { BsFillCaretDownFill, BsFillCaretUpFill } from 'react-icons/bs'
import './TableView.css'

function TableView(props) {
    let tableHeader = null
    let tableContent = null

    if (props.insertions['fields']) {
        let orderedFields = []

        // Defining the table header
        tableHeader = props.insertions['fields'].map((field) => {
            let fieldName = Object.keys(field)[0]
            orderedFields.push(fieldName)

            // Add automation of icon showing by passing the order-subject and return the special <th> only for that order
            // With if props.order-subject == fieldName
            if (fieldName === 'time') {

                let arrowType = props.orderType ? <BsFillCaretDownFill /> : <BsFillCaretUpFill />
                let fillArrow = props.orderBy === fieldName ? arrowType : null

                return (
                    <th
                        key={fieldName}
                        onClick={() => props.changeFetchSettings(null, null, fieldName, !props.orderType)}
                        style={{ cursor: 'pointer' }}
                    >
                        {groupNameFormatter(fieldName)}
                        {fillArrow}
                    </th>
                )
            }

            if (fieldName === 'title') {

                let arrowType = props.orderType ? <BsFillCaretDownFill /> : <BsFillCaretUpFill />
                let fillArrow = props.orderBy === fieldName ? arrowType : null

                return (
                    <th
                        key={fieldName}
                        onClick={() => props.changeFetchSettings(null, null, fieldName, !props.orderType)}
                        style={{ cursor: 'pointer' }}
                    >
                        {groupNameFormatter(fieldName)}
                        {fillArrow}
                    </th>
                )
            }

            return <th key={fieldName}>{groupNameFormatter(fieldName)}</th>
        })

        if (props.visualizationHint === 'map') {
            tableHeader.push(
                <th
                    onClick={() => props.changeFetchSettings(null, null, null, !props.orderType)}
                    style={{ cursor: 'pointer' }}
                    key={props.visualizationHint}
                >
                    <button
                        className="btn btn-sm btn-warning"
                        onClick={props.showAllOnMap}
                    >
                        Show all on Map
                    </button>
                </th>
            )
        }

        // Defining the table content
        tableContent = props.dataList.map((data) => {
            return (
                <TableRow
                    key={data['id']}
                    pk={data['id']}
                    openDeleteModal={props.openDeleteModal}
                    value={data['value']}
                    fields={orderedFields}
                    visualizationHint={props.visualizationHint}
                    clickedMap={props.clickedMap}
                />
            )
        })
    }

    return (
        <table className="table-view">
            <tbody>
                <tr>{tableHeader}</tr>
                {tableContent}
            </tbody>
        </table>
    )
}

export default TableView
