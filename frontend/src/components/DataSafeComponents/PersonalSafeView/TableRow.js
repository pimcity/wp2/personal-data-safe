import React from 'react'

function TableRow(props) {

    if (!props.value) return null;

    let valueContent = props.value[Object.keys(props.value)]

    // Allows to have many values or just one value
    let tableEntries = null
    if (typeof valueContent !== 'object') {
        tableEntries = <td>{valueContent}</td>
    } else {
        tableEntries = props.fields.map((key) => <td key={key}>{valueContent[key]}</td>)
    }

    let visualizationEntry = null
    switch (props.visualizationHint) {
        case 'map':
            if ((valueContent['latitude'] != null && valueContent['longitude'] != null)) {
                visualizationEntry = (
                    <td>
                        <button
                            className="btn btn-sm btn-success"
                            onClick={() => props.clickedMap([{lat: valueContent['latitude'], lng: valueContent['longitude']}])}
                        >
                            Open Map
                        </button>
                    </td>
                )
            }
            break

        default:
            break
    }

    const deleteEntry = <td><button className="btn btn-sm btn-danger" onClick={() => props.openDeleteModal(props.pk)}>Delete</button></td>

    return (
        <tr>
            {tableEntries}
            {visualizationEntry}
            {deleteEntry}
        </tr>
    )
}

export default TableRow
