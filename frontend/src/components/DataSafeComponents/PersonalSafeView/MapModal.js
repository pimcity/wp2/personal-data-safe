import React from 'react'
import CustomModal from '../../UI/CustomModal/CustomModal'
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api'

import './MapModal.css'

function calculateCenterCoordinates(coordinates) {
    let lat = 0
    let lng = 0
    let points = 0

    coordinates.forEach((point) => {
        lat += point.lat
        lng += point.lng
        points += 1
    })

    if (points === 0) {
        return [0, 0]
    }

    return [lat / points, lng / points]
}

export default function MapModal(props) {
    const { isLoaded, loadError } = useLoadScript({
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
    })

    let map = null
    if (loadError) {
        map = <p>{loadError}</p>
    } else if (!isLoaded) {
        map = <p>Loading....</p>
    } else {
        const mapContainerStyle = {
            width: '100%',
            height: '100%',
        }

        const [centerLat, centerLng] = calculateCenterCoordinates(props.coordinates)

        const center = {
            lat: centerLat,
            lng: centerLng,
        }

        const options = {
            disableDefaultUI: true,
            zoomControl: true,
        }

        map = (
            <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={3}
                center={center}
                options={options}
                clickableIcons={false}
            >
                {props.coordinates.map((point) => (
                    <Marker position={{ lat: point.lat, lng: point.lng }} key={Math.random() * 1000} />
                ))}
            </GoogleMap>
        )
    }

    return <CustomModal closeModal={props.closeModal}>{map}</CustomModal>
}
