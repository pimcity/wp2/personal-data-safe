import React, { useState } from 'react'

export default function PagesControls(props) {
    const [objsDisplayed, setObjsDisplayed] = useState(props.lengthInterval)

    const handleObjsDisplayed = (event) => {
        const { value } = event.target

        setObjsDisplayed(value)
    }

    const onEnterSubmit = (event) => {
        if (event.key === 'Enter') {
            props.changeFetchSettings(null, objsDisplayed)
        }
    }

    const nextButton = (
        <button
            className="btn btn-success mr-2"
            onClick={() => props.changeFetchSettings(null, null, null, null, props.currentPage + 1)}
            disabled={props.startInterval + props.returnedObjects >= props.totalObjs}
        >
            next
        </button>
    )

    const lastButton = (
        <button
            className="btn btn-success"
            onClick={() =>
                props.changeFetchSettings(null, null, null, null, Math.ceil(props.totalObjs / props.lengthInterval))
            }
            disabled={props.startInterval + props.returnedObjects >= props.totalObjs}
        >
            {Math.ceil(props.totalObjs / props.lengthInterval)}
        </button>
    )

    const firstButton = (
        <button
            className="btn btn-success mr-2"
            onClick={() => props.changeFetchSettings(null, null, null, null, 1)}
            disabled={props.currentPage <= 1}
        >
            1
        </button>
    )

    const previousButton = (
        <button
            className="btn btn-success mr-2"
            onClick={() => props.changeFetchSettings(null, null, null, null, props.currentPage - 1)}
            disabled={props.currentPage <= 1}
        >
            previous
        </button>
    )

    const max_val_input = `${props.totalObjs}`

    return (
        <>
            <div className="row h-100 align-items-center">
                <div className="col-12 col-md-3">
                    <div className="input-group h-100 align-items-center">
                        <input
                            type="number"
                            className="form-control rounded text-right p-1 w-sm-50"
                            value={objsDisplayed}
                            onChange={handleObjsDisplayed}
                            onKeyPress={onEnterSubmit}
                            min="1"
                            max={max_val_input}
                        />
                        <p className="m-0 mx-1 ">/{props.totalObjs}</p>
                        <div className="input-group-prepend d-flex justify-content-center align-items-center">
                            <button
                                className="btn btn-outline-secondary rounded"
                                type="button"
                                onClick={() => props.changeFetchSettings(null, objsDisplayed)}
                            >
                                change
                            </button>
                        </div>
                    </div>
                </div>
                <div className="buttons col-12 col-md-6 d-flex justify-content-center align-content-center h-50">
                    {firstButton}
                    {previousButton}
                    <p className="currentPage mr-3 ml-2 h-100">{props.currentPage}</p>
                    {nextButton}
                    {lastButton}
                </div>
                <div className="col-3"></div>
            </div>
        </>
    )
}
