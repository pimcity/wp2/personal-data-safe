import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { BsPeopleCircle } from 'react-icons/bs'
import { BoxArrowDownLeft } from 'react-bootstrap-icons'

import { groupNameFormatter } from '../../utility/formatters'
import './Sidebar.css'

function Sidebar(props) {
    // Redux
    const username = useSelector((state) => state.auth.username)

    return (
        <div className="sidebar-component p-4 d-flex flex-column justify-content-between">
            <div className="sidebar-top">
                <div className="user-card d-flex flex-column justify-content-around align-items-center">
                    <p className="user-icon mb-0">
                        <BsPeopleCircle />
                    </p>
                    <p>{username}</p>
                </div>

                <div className="sidebar-link text-center">
                    <NavLink exact to="/" activeClassName="is-active">
                        PDS Home
                    </NavLink>
                </div>

                {props.groups.map((group) => {
                    const linkName = '/' + group['group-name']

                    return (
                        <div className="sidebar-link text-center" key={group['group-name']}>
                            <NavLink exact to={linkName} activeClassName="is-active">
                                {groupNameFormatter(group['group-name'])}
                            </NavLink>
                        </div>
                    )
                })}
            </div>

            <Link
                to="/logout"
                className="logout-button align-self-center d-flex justify-content-center"
                style={{ textDecoration: 'none' }}
            >
                <button type="button" className="btn btn-outline-light " size="sm">
                    <strong className="mr-2">Logout</strong>
                    <BoxArrowDownLeft />
                </button>
            </Link>
        </div>
    )
}

export default Sidebar
