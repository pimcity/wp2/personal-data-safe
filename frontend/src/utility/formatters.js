export function groupNameFormatter(name) {
    try {
        let formattedName = name.replace('-', ' ')
        formattedName = formattedName.charAt(0).toUpperCase() + formattedName.slice(1)
        return formattedName
    } catch (err) {
        return name
    }
}

export function insertionsFormatter(insertions) {
    let insertion_dict = {}
    insertions.forEach((insertion) => {
        insertion_dict[insertion['name']] = {
            'data-type': fromTypetoInput(insertion['type']),
            value: '',
            original: '',
            id: null,
            error: null,
        }
    })

    return insertion_dict
}

export function fromTypetoInput(data_type) {
    switch (data_type){
        case 'date': return 'date'
        case 'int': return 'number'
        default: return 'string'
    }
}

export function base64ToBlob(base64, mimetype, slicesize) {
    if (!window.atob || !window.Uint8Array) {
        // The current browser doesn't have the atob function. Cannot continue
        return null;
    }
    mimetype = mimetype || '';
    slicesize = slicesize || 512;
    var bytechars = atob(base64);
    var bytearrays = [];
    for (var offset = 0; offset < bytechars.length; offset += slicesize) {
        var slice = bytechars.slice(offset, offset + slicesize);
        var bytenums = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            bytenums[i] = slice.charCodeAt(i);
        }
        var bytearray = new Uint8Array(bytenums);
        bytearrays[bytearrays.length] = bytearray;
    }
    return new Blob(bytearrays, {type: mimetype});
};

