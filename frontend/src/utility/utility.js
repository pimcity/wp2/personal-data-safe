export function isEmptyDict(obj) {
    return Object.keys(obj).length === 0
}