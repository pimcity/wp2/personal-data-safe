export default function validateAuthForm(values) {
    let errors = {}

    if (!values.username.trim()){
        errors.username = "Username required"
    }

    if (!values.password.trim()){
        errors.password = "Password is required"
    } else if (values.password.length < 5) {
        errors.password = "Password needs to be 5 characters or more"
    }

    return errors
}