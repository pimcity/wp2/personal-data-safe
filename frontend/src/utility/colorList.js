export let colorList = [
    {
        "hex": "#FF0000", 
        "name": "Red", 
        "rgb": "(255,0,0)"
    }, 
    {
        "hex": "#6eb004", 
        "name": "Olivedrab", 
        "rgb": "(110,176,4)"
    }, 
    {
        "hex": "#0000FF", 
        "name": "Blue", 
        "rgb": "(0,0,255)"
    }, 
    {
        "hex": "#FF00FF", 
        "name": "Magenta", 
        "rgb": "(255,0,255)"
    }, 
    {
        "hex": "#808080", 
        "name": "Grey", 
        "rgb": "(128,128,128)"
    }, 
    {
        "hex": "#800000", 
        "name": "Maroon", 
        "rgb": "(128,0,0)"
    }, 
    {
        "hex": "#008000", 
        "name": "Green", 
        "rgb": "(0,128,0)"
    }, 
    {
        "hex": "#FF4500", 
        "name": "Orange-red", 
        "rgb": "(255,69,0)"
    }, 
    {
        "hex": "#008080", 
        "name": "Teal", 
        "rgb": "(0,128,128)"
    }, 
    {
        "hex": "#0000FF", 
        "name": "Pink", 
        "rgb": "(255,182,193)"
    }, 
]

export function rgbToRgba(rgbColor, alpha){
    const alphaString = alpha.toString()
    return rgbColor.replace(")", `, ${alphaString})`)
}

export function randomRGBAcolor(alpha){
    if (typeof(alpha) !== 'number') {
        throw 'alpha is not a number';
    }

    let color = colorList[Math.floor(Math.random() * colorList.length)];

    return rgbToRgba(color['rgb'], alpha)
}

export function indexColor(index, alpha){
    let color = colorList[index]

    return rgbToRgba(color['rgb'], alpha)
}