// PAckages
import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'
import {Provider} from 'react-redux'

// Components
import App from './App'

// Redux Store
import store from './store/store'

// CSS
import './index.css'

const app = (
    <Provider store={store}>
        <React.StrictMode>
            <App />
        </React.StrictMode>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'))