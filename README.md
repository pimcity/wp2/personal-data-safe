Personal Data Safe
==================


# 1. Intro

The Personal Data Safe (P-DS) is the means to store personal data in a controlled form. It implements a secure repository for the user's personal information like navigation history, contacts, preferences, personal information, etc. It gives the possibility to handle them though REST-based APIs or a web interface. Thanks to the REST APIs, the P-DS can be accessed also by other components of the PDK. The architecture of the PDK is depicted in the figure below.

![image info](https://gitlab.com/pimcity/wp2/personal-data-safe/-/raw/master/pds.png)


# 2. Installation

The documentation and the following instructions refer to a Linux environment (Ubuntu has been used for testing), with **Python 3.8.2** and **pip 20.0.2** installed. The P-DS project has been cloned from [this GitLab repository](https://gitlab.com/pimcity/wp2/personal-data-safe.git).

Follow accurately the next steps to quickly set-up the P-DS on your server. The package comes with a frontend and a backend already implemented, but if the needs calls only for a ready API you can cancel the folder  '/frontend' and skip the relative steps. All relevant steps are designed for a Linux machine, perform the equivalent procedure with other environments.

##  First Steps
Update all packages:
> sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade

Install pip/pip3:
> sudo apt-get python3-pip

Import the project from the GIT repository:
> git clone https://gitlab.com/pimcity/wp2/personal-data-safe.git

## Backend setup
Now enter the backend folder:
> cd backend/

Create a python virtual environment and activate it:
> python3 -m venv venv
source venv/bin/activate

Install the python-dev library and also add wheel:
> sudo apt-get install libpq-dev python-dev
pip install wheel

Install all requirements:
> pip install -r requirements.txt

Now we need to set-up the database, for this project the default one is PostegreSQL. If there is no need to change database, then the app will be ready to use after the next steps. Instead, if another database is needed, check the django documentation [here](https://docs.djangoproject.com/en/3.2/ref/databases/#:~:text=Django%20officially%20supports%20the%20following,MySQL) to find out what steps to follow.

- First install PostegreSQL:
> sudo apt-get update
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib

- Create a database and database user. 
The default settings are:
> db_name = pds_postgres 
db_username = admin
user_password = admin_secret_password

- The steps to create the new database are the following:
> sudo su - postgres
psql
CREATE DATABASE <db_name>;
CREATE USER <db_username> WITH PASSWORD '<user_password>';
ALTER ROLE <db_username> SET client_encoding TO 'utf8';
ALTER ROLE <db_username> SET default_transaction_isolation TO 'read committed';
ALTER ROLE <db_username> SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE <_db_name_> TO <db_username>;
\q
exit

- It is now possible to use the database using the user and credentials registered:
> psql --host \<localhost or ip_addr> --port \<port num> --username \<db_username> \<_db_name_>

Connect Django to the PostgreSQL 
> pip install django psycopg2

If you don't want to use the default username and password, you will need to change them in the settings in the project file `backend/config/settings.py`. 
```
DATABASES = {
	'default': {
	'ENGINE': 'django.db.backends.postgresql_psycopg2',
	'NAME': '<_db_name_>',
	'USER': '<db_username>',
	'PASSWORD': '_<user_password>_',
	'HOST': 'localhost',
	'PORT': '',
	}
}
```

Now Migrate the database. NB that it's better and safer to perform migrations for each app:
>python manage<span>.py makemigrations app_users
python manage<span>.py makemigrations app_personal_data
python manage<span>.py migrate

Create a Django SuperUser, which will also work as admin in the admin page:
> python manage<span>.py createsuperuser

It is now possible to run server and access admin:
>python manage<span>.py runserver 0.0.0.0:8000
admin found @ localhost:8000/admin

## Frontend setup
This section focuses on the frontend setup of the application. The frontend is developed in javascript using the ReactJS framework. To begin, enter the frontend folder:
> cd frontend/

First install NodeJs.
- Enable the NodeSource repository by running the following curl command as a user with sudo privileges:
> curl -sL http<span>s://deb.nodesource.com/setup_12.x | sudo -E bash -

- Once the NodeSource repository is enabled, install Node.js and npm by running:
> sudo apt install nodejs

- Verify that the Node.js and npm were successfully installed by printing their versions:
> node --version

> npm –version

Install dependencies for React:
> cd Frontend
npm install

In case of some error like `"This version of npm is compatible with lockfileVersion@1, but package-lock.json was generated for lockfileVersion@2. I'll try to do my best with it!”` try running this command:
> sudo rm -rf node_modules package-lock.json && npm install

Run React Server:
>npm start

The project should now be working. Activate concurrently the Django and the ReactJS server and the platform should be fully functional.

## Test Data
If needed some scripts are already present for quickly creating some test data and check that the platform is correctly working.
They are `script_create_random_user.py`, `script_create_random_browsing_history.py` and `script_create_random_location_history.py`.

### Random users
To create random users use the script `create_random_user.py`. This script will generate a given amount of users, with a random username and a fixed password: `test_password`. Only need to keep track of the usernames created. Users will have some personal fields already randomly generated.

Usage:
>python create_random_user.py \<int:number_of_new_users>

Example of usage:
> python create_random_user.py 1

Example of output:
>{'username': 'isabella_wilson'}
{'id': 43280, 'value': {'birth-date': '1999-03-16'}, 'metadata': 'metadata-birth-date', 'data_type': 'birth-date', 'group_name': 'personal-details', 'description': 'description-birth-date', 'created': '2021-05-03T14:42:27.345054Z'}

### Random browsing history
To generate random browsing history data for a specific user, use the script `create_random_browsing_history.py`. This script will generate a given amount of browsing history data, and assign them to a given user.

Usage:
>python create_random_browsing_history.py \<str:username> \<int:number_of_new_data>

Example of usage:
>python create_random_browsing_history.py isabella_wilson 2

Example of output:
>{'id': 43281, 'value': {'visited-url': {'url': 'http://www.msn.com', 'title': 'Msn', 'time': '2016-03-06 04:58:20'}}, 'metadata': 'metadata-visited-url', 'data_type': 'visited-url', 'group_name': 'browsing-history', 'description': 'description-visited-url', 'created': '2021-05-03T14:48:16.934886Z'}
{'id': 43282, 'value': {'visited-url': {'url': 'http://www.onet.pl', 'title': 'Onet', 'time': '2018-08-29 03:49:59'}}, 'metadata': 'metadata-visited-url', 'data_type': 'visited-url', 'group_name': 'browsing-history', 'description': 'description-visited-url', 'created': '2021-05-03T14:48:16.941765Z'}

### Random location history
To generate random location history data for a specific user, use the script `create_random_location_history.py`. This script will generate a given amount of location history data, and assign them to a given user.

Usage:
>python create_random_location_history.py \<str:username> \<int:number_of_new_data>

Example of usage:
>python create_random_location_history.py isabella_wilson 2

Example of output:
>{'id': 43286, 'value': {'visited-location': {'latitude': 46.05635313711818, 'longitude': 12.831298674353643, 'time': '2010-03-04 04:45:20', 'description': 'example'}}, 'metadata': 'metadata-visited-location', 'data_type': 'visited-location', 'group_name': 'location-history', 'description': 'description-visited-location', 'created': '2021-05-03T14:49:59.668810Z'}
{'id': 43287, 'value': {'visited-location': {'latitude': 43.880077139481415, 'longitude': 15.797250794542236, 'time': '2012-07-28 10:55:07', 'description': 'example'}}, 'metadata': 'metadata-visited-location', 'data_type': 'visited-location', 'group_name': 'location-history', 'description': 'description-visited-location', 'created': '2021-05-03T14:49:59.675079Z'}

# 3. Usage

## Data Model

The data model of the application consists of 2 main classes:

-   User class
-   PersonalInformation class

The User class is used to store the information about the single user and it extends the _AbstractUser_ class provided by Django. The class does not modify much the parent class, since it doesn't need to store additional information. Each user is linked to a set of personal information; the information about this relationship can be found in the PersonalInformation class.

On the other side, personal information are modeled by the _PersonalData_ class that is used to store arbitrary types of data, e.g., user static information (name, surname, year of birth, email, etc.), browsing history, location history. The _value_ field of the PersonalData class is defined as a _JSONField_ object; in this way, user can store both elementary data, such as int, string, etc., and more structured data, such as dictionaries, that are represented as JSON objects. The PersonalData class has also _user_ attribute that is defined as a _ForeignKey_ object, so that each PersonalData instance has a reference to the user that owns the entry. This solution emulates what is done in a classic relational system and it has been chosen because it proved to be the most efficient especially with big volumes of data. Others field of the PersonalData class provides additional information related such as:

-   group_name: it's the semantic group the entry belongs to.
-   metadata: it's the name that identifies a personal information inside a group
-   type: it's the type of the information. Possible types are int, string, date, boolean, float, dict. Since each entry is modeled as a JSON object, elementary type information are stored in the form `{'type': value}`, while dict information in the form `{'subfield1': value1, 'subfield2', value2}`.

## Schema
The schema is stored in the data safe file system and loaded at application initialization, specific functions ensure that the schema defined follows some basics guidelines that will be defined later in this paragraph. The whole project logic has been developed prioritizing ease of usage: the main goal is to only change/modify the schema for the whole application (backend and frontend) to adapt to the change.
The schema defines the kind of information that the data safe can accept; each information is characterized by the following basic attributes in the schema:

-   **group-name**: it defines the high level group that the data belongs to, such as _personal-details, _browsing-history_, ect.
-   **name**: it's the name that identifies the information inside a group, e.g. _birth-date_ in the _personal-details group
-   **type**: it defines the type associated to the information, e.g. _birth-date_ must be a Date

The schema is used to validate the input provided by the user, in order to control that the inserted data complies with the information that the P-DS can store. The schema can be updated if the user wants to store additional information: new types must be declared using the same name-type syntax used for old types, so that the application can handle changes in the schema.

The schema is used also to read data from the database: data that no longer match the schema are simply ignored, so that users can still have access to them (returning to an old version of the schema for example).

```yaml
name: "PIMCity default PDS schema"
version: 0.2
author: "Federico Torta, Annaloro Enrico, Martino Trevisan"
content:
- group-name: personal-details (1)
  user-insertion: true (2)
  user-update: true (3)
  add-zip-file: false (4)
  extract-json: true (5)
  types: (6)
    - name: first-name
      type: string
    - name: last-name
      type: string
    - name: birth-date
      type: date
    - name: age
      type: int
- group-name: browsing-history (7)
  user-insertion: false
  user-update: false
  add-zip-file: true
  extract-json: true
  types:
    - name: visited-url
      historical: true (8)
      type: dict (9)
      fields: (10)
        - url: string
        - page-title: string
        - time: date
- group-name: location-history (11)
  user-insertion: false
  user-update: false
  visualization-hint: map (12)
  add-zip-file: true
  extract-json: true
  types:
    - name: visited-location
      historical: true
      type: dict
      fields:
        - latitude: float
        - longitude: float
        - description: string
        - time: date
```

**Explanation**
 - Each *group-name* field represents a possible semantic high-level group. In this case the first group accepted by the P-DS is **personal-details** (1). The possible data that are considered part of the personal-details group are defined in the *types* field (6) which lists all the variants of a personal-details entry. The *types* field is basically a list of *name*-*type* pairs where *name* is the common name of the entry, while *type* is the actual type of the information. Each data is represented with the name and the type in order to let the P-DS store any kind of information, without being tied to a particular elementary type. The user can store any data he want but the P-DS will cast the inserted data to control the type compliance. 
Moreover each group can presents additional settings:
 - *user-insertion* (2): if this field is true, the user will be able to add the information manually. The frontend will show an **Add** button that allows the user to directly insert new data from the page. Schema and type compliance controls are executed.
 - *user-update* (3): if true, the user can update a P-DS entry from the UI, using an **Edit** button that allows to change just the value of the stored data.
 - *add-zip-file* (4): if true, the user can add new data from a zip file, which contains a JSON file with the information to be inserted. The zip file is uploaded using an **Upload ZIP file** button of the UI. This function is just a prototype and aims to show the potentiality of the P-DS: the data import feature will be presumably used by automatic software systems that can create the correct JSON file, giving the possibility to import user data (even in big volumes) from other data stores.
 - *extract-json* (5): if true, the user can click an **Extract data** button to download the stored data in JSON format, inside a zip file. As well as the data import functionality, also the data extraction can be performed both at global level (all the groups together) or at group-level (so download just the data related to a certain group).
 - (8) and (11) means that the other possible groups that the P-DS supports are *location-history* information and *browsing-history* information.
 - some information types can be stored multiple times, because they are linked to a particular timestamp, such visited urls or the visited locations. In this case they are enriched with the flag *historical*, so that the user can have multiple entries for that type.
 - in order to let the user store more complex and structured data, the schema supports the *dict* type (10). A dict object is basically a JSON object with a set of key-value pair. Each entry of the dict is a piece of the complete information and is represented as a name-value pair just as the elementary entry of the P-DS. In this way the same controls can be applied recursively on the elements of a dict object. The single component of a dict are displayed in the *fields* key (11).
 - The visualization-hint (12) is a special property used for visualization aids in the frontend. Some keywords are mandatory and additional set-up in the fields will be mandatory to support the feature. As of today the active features are:
	
	 - `map` :  allows to plot, in a google maps canvas, one or more points given its coordinates. For this reason, when this field is set, it must also be set up two mandatory fields (10) `latitude` and `longitude`, as shown in the example above. Failing to do so will prevent the application from starting.

 - On the frontend, it has also been implemented a sorting logic to order items based on a specific field. To automate this we introduced a set of fields which support this feature, this means that, by using specific names for the fields, some special features (such as ordering) will be available. The fields are the following

	 - `time: date` : ascending and descending sort based on date
	 - `title: string`: ascending and descending sort based on alphabetical order

```yaml
# This example can be sorted both by time and by title
- group-name: browsing-history
  user-insertion: false
  user-update: false
  add-zip-file: true
  extract-json: true
  types:
    - name: visited-url
      historical: true
      type: dict
      fields:
        - url: string
        - title: string # <-- here
        - time: date # <-- here
``` 

## Web API

The specifications of the P-DS Web API can be found in the OpenAPI format on GitLab, at [this link](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP2/personal-data-safe.yml). The API allow other components, on behalf of the user, to create, read, update and delete personal information.

Requests shall be authenticated via the `Authorization: Bearer` HTTP Header. The Token can refer to a local user of the PDS: in this case, the token can be generated via username and password using the above mentioned API. The token can also be a JWT generated using a configurable OAuth provider (parameters are in the file `/backend/config/settings.py`). In this case, the access token must have the four scopes: `read:pds create:pds update:pds delete:pds`.

## Data buyer API

This section describes the functionalities and requirements for the correct operation of the databuyers API.

The databuyers section provides the users information to the different databuyers. Each request must be authenticated via a JWT access token obtained via an OAuth procedure (configurable in the file `/backend/config/settings.py`). The token must have the scope `databuyer:pds`.
Requests are made to the enpoint `/data-buyers/get-data/` using the POST method, and  the payload must contain the indication of the user and type of personal information to retrieve. Details and examples are available in the OpenAPI documentation available at [this link](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP2/personal-data-safe.yml).


# 4. License

The P-DS is distributed under AGPL-3.0-only, see the `LICENSE` file.

  Copyright (C) 2021  Politecnico di Torino - Martino Trevisan, Marco Mellia,
                                              Nikhil Jha, Luca Vassio,
                                              Federico Torta, Enrico Annaloro
