from django.contrib import admin
from .models import PersonalData


class PersonalDataAdminConfig(admin.ModelAdmin):
    '''
    Custom Admin page config to better display information in the admin page
    '''
    list_display = ('id', 'metadata', 'group_name', 'data_type', 'owner')
    readonly_fields=('owner', 'metadata', 'group_name', 'data_type', 'value', 'description')


admin.site.register(PersonalData, PersonalDataAdminConfig)