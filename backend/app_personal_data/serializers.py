from rest_framework import serializers
from .models import PersonalData
from django.utils import timezone


class PersonalDataSerializer(serializers.Serializer):

    id = serializers.IntegerField(read_only=True)
    value = serializers.JSONField()
    metadata = serializers.CharField(max_length=100, allow_blank=True)
    data_type = serializers.CharField(max_length=100)
    group_name = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=300, allow_blank=True)
    created = serializers.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['created']

    def create(self, validated_data):
        """
        Create and return a new `PersonalData` instance, given the validated data.
        """
        PD = PersonalData.objects.create(**validated_data)
        return PD

    def to_dict(self):
        temp_dict = {}
        temp_dict['id'] = self.id
        temp_dict['value'] = self.value
        temp_dict['metadata'] = self.metadata
        temp_dict['group-name'] = self.group_name
        temp_dict['description'] = self.description
        return temp_dict