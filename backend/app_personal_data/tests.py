from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APIClient

import datetime
import json

from .models import PersonalData


class PersonalDataTests(TestCase):
    '''
    Test set on the model and saving methods of PI
    '''


    # @classmethod means: when this method is called, we pass the class as the first
    # argument instead of the instance of that class (as we normally do with methods).
    # This means you can use the class and its properties inside that method rather
    # than a particular instance.
    @classmethod
    def setUpTestData(cls):

        # Create a user
        test_user1 = User.objects.create_user(
            username='test_user1',
            password='test_password1'
        )
        test_user1.save()

        # Create personal data
        test_value = json.dumps({
            "var1": "value1",
            "var2": "value2",
            "var3": "value3"
        })
        test_metadata = 'visited-url'
        test_data_type = 'type'
        test_group_name = 'group-name'
        test_description = 'description'
        test_created = datetime.datetime(2020, 5, 17, 0, 0)

        test_PD = PersonalData.objects.create(
            value=test_value,
            metadata=test_metadata,
            data_type=test_data_type,
            group_name=test_group_name,
            description=test_description,
            created=test_created,
            owner=test_user1,
        )
        test_PD.save()

        cls.test_PD_id = test_PD.id

    def test_PD_content(self):
        '''
        This function tests that the content inserted in the set up corresponds to the
        one actually saved.
        '''

        # Retrieving the PI object
        test_PD = PersonalData.objects.get(id=self.test_PD_id)

        # Setting up test values
        test_value = json.dumps({
            "var1": "value1",
            "var2": "value2",
            "var3": "value3"
        })
        test_metadata = 'visited-url'
        test_data_type = 'type'
        test_group_name = 'group-name'
        test_description = 'description'
        test_created = datetime.datetime(
            2020, 5, 17, 0, 0).strftime("%Y-%m-%d %H:%M:%S")

        # Performing tests
        self.assertEqual(f'{test_PD.value}', test_value)
        self.assertEqual(f'{test_PD.metadata}', test_metadata)
        self.assertEqual(f'{test_PD.data_type}', test_data_type)
        self.assertEqual(f'{test_PD.group_name}', test_group_name)
        self.assertEqual(f'{test_PD.description}', test_description)
        self.assertEqual(f'{test_PD.created}'[:-6], test_created)


class PersonalDataAPITests(TestCase):
    '''
    Test set on the API usage and the requests made by users
    '''

    # Setting up test values
    test_value = json.dumps({
        "var1": "value1",
        "var2": "value2",
        "var3": "value3"
    })
    test_metadata = 'visited-url'
    test_data_type = 'type'
    test_group_name = 'group-name'
    test_description = 'description'
    test_created = datetime.datetime(2020, 5, 17, 0, 0)

    data = {
        "value": test_value,
        "metadata": test_metadata,
        "data_type": test_data_type,
        "group_name": test_group_name,
        "description": test_description,
        "created": test_created
    }

    def test_create_PD(self):
        """
        Ensure we can create a new Personal Data object and view the object.
        """

        # Create a user
        self.test_user1 = User.objects.create_user(username='test_user1',
                                                   password='test_password1')
        self.test_user1.save()

        # Acting like a client with the client API test
        client = APIClient()
        client.login(username=self.test_user1.username,
                     password='test_password1')

        url = reverse('personal_data:list_create')
        response = client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_edit_PD(self):
        '''
        Ensure only owner can update and view its Personal Informations
        '''

        # Create two users
        self.test_user1 = User.objects.create_user(username='test_user1',
                                                   password='test_password1')
        self.test_user1.save()

        self.test_user2 = User.objects.create_user(username='test_user2',
                                                   password='test_password2')
        self.test_user2.save()

        # Creating a test PI based on test_user1
        test_PD = PersonalData.objects.create(
            value=self.test_value,
            metadata=self.test_metadata,
            data_type=self.test_data_type,
            group_name=self.test_group_name,
            description=self.test_description,
            created=self.test_created,
            owner=self.test_user1,
        )

        # Acting like test_user2 with the client API test
        client = APIClient()
        client.login(username=self.test_user2.username,
                     password='test_password2')

        # Setting up the specific url with the newly created PI
        url = reverse('personal_data:detail_edit',
                      kwargs={'pk': test_PD.id})

        # Creating a get request and checking if the response is forbidden as it should be
        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Creating some edited data
        edit_data = {
            "test_metadata": 'visited-url (edit)',
            "test_data_type": 'type (edit)',
        }

        # Creating a put request and checking if the response is forbidden as it should be
        response = client.put(url, edit_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
