from rest_framework.permissions import BasePermission, SAFE_METHODS, IsAuthenticated

class PersonalInformationPrivacyPermission(IsAuthenticated):
    '''
    This permission allows actions only from the user that owns the informations
    '''
    message = 'Viewing or manipulating personal infromation is restricted to the owners only.'

    def has_object_permission(self, request, view, obj):

        # Otherwise for all other methods we check if the PersonalInformation
        # user is the same as the user that made the request
        return obj.owner == request.user
