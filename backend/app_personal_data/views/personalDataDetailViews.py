# Packages imports
from django.core import serializers
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

import inspect

# Local files import
from ..models import PersonalData
from ..permissions import PersonalInformationPrivacyPermission
from ..serializers import PersonalDataSerializer
from ..utils.validation_utils import isMacroGroup, isEditableData, isHistoricalData, isUserInsertionData, validateUserData


class PersonalDataDetailAPIView(APIView):
    permission_classes = [PersonalInformationPrivacyPermission]

    def get(self, request, pk, format=None):
        '''
        This function returns the single personal-data instance of a specific user defined by the 
        its private key in the database.
        '''

        try:
            user = request.user
            personalDatas = PersonalData.objects.filter(owner=user,
                                                        pk=pk)

            serializer = PersonalDataSerializer(personalDatas, many=True)

            if len(serializer.data) < 1:
                return Response({"error": f"The personal data with id {pk} could not be found"}, status=status.HTTP_404_NOT_FOUND)

            return Response(serializer.data[0], status=status.HTTP_200_OK)

        except Exception:
            return Response({"error": "Could not get the stored data from database"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response({"error": "Could not get the stored data from database"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, pk=None, format=None):
        '''
        This function tries to update an instance of a personal data defined by its pk.
        It also returns the updated instance.
        '''

        # First we get the object via its pk
        try:
            user = request.user
            personalDatas = PersonalData.objects.filter(owner=user,
                                                        pk=pk)

            value_name = request.data['value-name']
            value_new = request.data['value-new']

            if not value_name or not value_new:
                raise Exception

            serializer = PersonalDataSerializer(personalDatas, many=True)

        except Exception:
            return Response({"error": "Could not set up procedure correctly, check that all values are correct"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Cast data to dict for integrity check
        if len(serializer.data) < 1:
            return Response({"error": f"The personal data with id {pk} could not be found"}, status=status.HTTP_404_NOT_FOUND)

        data = dict(serializer.data[0])

        # Check if data is editable
        if not isEditableData(user, data['group_name'], list(data['value'].keys())[0], pk):
            return Response({"error": "Update could not be performed, data is not editable"}, status=status.HTTP_400_BAD_REQUEST)

        # Data exists and can be edited
        try:

            value = data['value']

            if value_name not in value:
                return Response({"error": f"value-name '{value_name}' is not modifiable, because it does not exist"}, status=status.HTTP_400_BAD_REQUEST)

            value[value_name] = value_new

            PersonalData.objects.filter(pk=pk).update(value=value)
            modified_data = PersonalData.objects.filter(owner=user,
                                                        pk=pk)
            serializer = PersonalDataSerializer(modified_data, many=True)

            return Response(dict(serializer.data[0]), status=status.HTTP_200_OK)

        except Exception:
            return Response({"error": "Could not update successfully"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request, pk, format=None):
        '''
        This function deletes a single instance of a personal data, given its PK.
        '''

        try:
            user = request.user
            personalDatas = PersonalData.objects.filter(owner=user,
                                                        pk=pk)
        except Exception:
            error_message = "Error while retrieving the object from database, please try again"
            return Response({"error": error_message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        if not personalDatas:
            error_message = "Personaldata object not found, please check that the ID is correct and the user owns this PD"
            return Response({"error": error_message}, status=status.HTTP_400_BAD_REQUEST)

        try:
            personalDatas.delete()
        except Exception:
            error_message = "Error occurred while deleting personal data object, please try again"
            return Response({"error": error_message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)
