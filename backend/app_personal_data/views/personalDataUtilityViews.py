# Packages import
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.contrib.auth import get_user_model
from django.db.models import Count
from django.db.models.functions import TruncDay

# Local imports
from ..apps import schema_yaml
from ..models import PersonalData
from ..permissions import PersonalInformationPrivacyPermission


@api_view(['GET'])
@permission_classes((AllowAny,))
def apiOverview(request):
    '''
    This utility function returns a dictionary of all the possible routes accessible in the API
    '''

    api_urls = {
        "List": "/personal-data/",
        "Create": "/personal-data/",
        "List": "/personal-data/<str:macro_group>/",
        "Detail": "/personal-data/<int:pk>/",
        "List": "/utility/macro-groups/",
        "List": "/utility/user-insertions?group-name=<macro-group>",
    }

    return Response(api_urls, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def getMacroGroups(request):
    '''
    This utility function returns a list of objects containing the macro groups defined in the schema.yaml file.
    Each object fully describes the macro group properties.
    '''

    try:
        content = schema_yaml['content']

        macro_groups = []
        for group in content:
            macro_groups.append(group)

        return Response(macro_groups, status=status.HTTP_200_OK)

    except Exception:
        return Response({"error": "Something went wrong..."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes((AllowAny,))
def getUserInsertions(request):
    '''
    This utility function gets a query GET request with parameter 'group-name' and returns a list of dictionaries
    describing all the possible values that the user can insert in manually in the defined macro group.

    ex. QUERY: '?group-name=personal-information'
        OUTPUT: [
                    {
                        "name": "first-name",
                        "historical": false,
                        "type": "string"
                    },
                    {
                        "name": "last-name",
                        "historical": false,
                        "display-name": "last name",
                        "type": "string"
                    },
                ]
    '''

    # Check if the query param is set
    try:
        group_name = request.GET['group-name']

    except Exception:
        return Response({"error": "could not get query param 'group-name'"}, status=status.HTTP_400_BAD_REQUEST)

    # Find chosen group, extract info and returns
    try:
        content = schema_yaml['content']

        user_insertions = []
        found_group = None
        for group in content:
            if group_name == group['group-name'] and group['user-insertion']:
                found_group = group

        if not found_group:
            return Response({"error": "chosen group was not found..."}, status=status.HTTP_400_BAD_REQUEST)

        try:
            for user_insertion in found_group['types']:
                user_insertions.append(user_insertion)
        except Exception:
            return Response({"error": "Something went wrong..."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(user_insertions, status=status.HTTP_200_OK)

    except Exception:
        return Response({"error": "Something went wrong..."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes((PersonalInformationPrivacyPermission,))
def getUserGraphsData(request):
    '''
    This utility function returns informations about the user data contained in the database such as quantity, dates, ...
    '''

    username = request.user
    user = get_user_model().objects.get(username=username)
    personalDatas = PersonalData.objects.filter(owner=user).values(
        'group_name').annotate(total=Count('group_name'))

    personalDataTimes = (PersonalData.objects
                         .filter(owner=user)
                         .annotate(day=TruncDay('created'))
                         .values('day')
                         .annotate(total=Count('id'))
                         .order_by('day')
                         .values('day', 'total')
                         )

    data = {
        "result": "OK",
        "macro-groups": list(personalDatas),
        "data-days": list(personalDataTimes)
    }

    return Response(data, status=status.HTTP_200_OK)
