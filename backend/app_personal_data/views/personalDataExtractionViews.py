# Packages Imports
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Count
from django.contrib.auth import get_user_model
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from datetime import datetime
from io import BytesIO
import zipfile
import pathlib
import shutil
import base64
import json
import os

# Local files import
from ..models import PersonalData
from ..serializers import PersonalDataSerializer
from ..permissions import PersonalInformationPrivacyPermission
from ..utils.query_utils import getPersonalDataQuery
from ..apps import schema_yaml
from .personalDataViews import send_to_task_manager


class PersonalDataExtractionAPIView(APIView):

    permission_classes = [PersonalInformationPrivacyPermission]

    def create_json_file(self, username, group_name):
        '''
        This function create the JSON file for data extraction
        '''

        user = get_user_model().objects.get(username=username)
        objects_total, output_data = getPersonalDataQuery(
            user=user, category=group_name)

        with open("archive/" + group_name + ".json", "w") as outfile:
            json_obj = json.dumps(output_data, indent=4)
            outfile.write(json_obj)

        return outfile

    def get_user_group_names(self, username):
        '''
        This function returns a list of group names where at least one personal-data is present for a specific user
        '''
        group_names = []
        user = get_user_model().objects.get(username=username)
        personalDatas = PersonalData.objects.filter(owner=user).values(
            'group_name').annotate(total=Count('group_name'))

        group_names = [group_name['group_name']
                       for group_name in list(personalDatas)]

        return group_names

    def get(self, request, format=None):
        '''
        This function gets all user personal data and returns them as a zip file.

        Personal data can also be filtered with url queries.
        Queries:
            - "group_names[]": one, or more, of the macro-groups in the schema file (ex personal-information). Default is ALL.
        '''
        username = request.user
        group_names = request.GET.getlist("group-names[]", None)

        if username == None:
            error_message = "User not found..."
            return Response({"result": "FAIL", "error_message": error_message}, status=status.HTTP_400_BAD_REQUEST)

        if len(group_names) == 0:
            # If no group name is chosen, we then set all group_names that the user has data in
            group_names = self.get_user_group_names(username)
        else:
            # If its not empty then we check that for each of the passed categories the user has some personal-data
            user_group_names = self.get_user_group_names(username)
            group_names = [
                group_name for group_name in group_names if group_name in user_group_names]

        # After all the formatting if none of the group_names is present then we return the error
        if len(group_names) == 0:
            error_message = "Either user has no personal-data, or the group_name selection is wrong. Please check accordingly"
            return Response({"result": "FAIL", "error_message": error_message}, status=status.HTTP_400_BAD_REQUEST)

        try:
            # Create in-memory zip file
            os.makedirs("archive")
            in_memory = BytesIO()
            zipObj = zipfile.ZipFile(in_memory, mode='w')

            for group in group_names:
                # Create json file and add to zip file
                json_file = self.create_json_file(username, group)
                zipObj.write(json_file.name)

            # Add schema
            with open("archive/schema.json", "w") as outfile:
                schema_json_obj = json.dumps(schema_yaml, indent=4)
                outfile.write(schema_json_obj)

            zipObj.write(outfile.name)
            zipObj.close()

            # send the data back to the client; the zip file is base64-encoded
            in_memory.seek(0)
            zip_file_read = in_memory.read()
            zip_encode64 = base64.encodebytes(zip_file_read)
            data = {
                'result': 'OK',
                'download': {
                    'mimetype': 'application/zip',
                    'filename': 'archive.zip',
                    'data': zip_encode64
                }
            }

            shutil.rmtree(os.path.join(pathlib.Path().absolute(), "archive"))

            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({"result": "FAIL", "error_message": e}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        '''
        PROTOTYPE FUNCTION---

        This function add the information contained inside a zip file, uploaded by the user.
        '''

        # Retrieving data        
        group_names = request.POST.get('group_names', None)
        zip_file = request.FILES.get('zip', None)
        username = request.user
        user = get_user_model().objects.get(username=username)

        # Checking if essential data is present, otherwise return error
        if(group_names == None):
            error_message = "Missing group_names"
            return Response({"result": "FAIL", "error_message": error_message}, status=status.HTTP_400_BAD_REQUEST)

        if(zip_file == None):
            error_message = "Missing zip_file"
            return Response({"result": "FAIL", "error_message": error_message}, status=status.HTTP_400_BAD_REQUEST)

        # Innitialization of variables for data extraction
        # Each data extraction type must have its own variable
        json_file_bh = None
        json_file_lh = None

        # Reading files and saving wanted data
        with zipfile.ZipFile(zip_file, 'r') as z:
            for filename in z.namelist():
                if("BrowserHistory.json" in filename and 'browsing-history' in group_names):
                    with z.open(filename) as f:
                        data = f.read()
                        json_file_bh = json.loads(data.decode("utf-8"))
                elif("Location History/Records.json" in filename and 'location-history' in group_names):
                    print("Found location-history in ZIP file")
                    with z.open(filename) as f:
                        data = f.read()
                        json_file_lh = json.loads(data.decode("utf-8"))

        # We use this variable to keep track of the successfully uploaded personal-data
        results_info = []

        # Add information for browsing-history
        if ('browsing-history' in group_names):
            try:
                group_name = 'browsing-history'

                if json_file_bh is None:
                    results_info.append({'name': group_name, 'result': 'FAIL',
                                         'error': 'Could not upload given data, the realtive file is not present in the zip'})

                else:
                    browsing_history = json_file_bh['Browser History']
                    for entry in browsing_history:
                        url = entry['url']
                        title = entry['title']
                        time = float(entry['time_usec'])
                        time_ = time / 1000000.0
                        time = datetime.fromtimestamp(
                            time_, tz=timezone.utc).strftime("%Y-%m-%d %H:%M:%S")

                        value = {
                            "visited-url": {
                                'url': url.rstrip(),
                                'title': title.rstrip(),
                                'time': time
                            }
                        }

                        data = {
                            "data_type": 'visited-url',
                            "value": value,
                            "group_name": group_name,
                            "metadata": 'visited-url',
                            "description": 'imported zip data'
                        }

                        newDataSerializer = PersonalDataSerializer(data=data)

                        if (newDataSerializer.is_valid()):
                            newDataSerializer.save(owner=user)

                    # If all uploads are succesfull, we can add the category to the successful_uploads
                    results_info.append({'name': group_name, 'result': 'OK'})

            except Exception:
                results_info.append({'name': group_name, 'result': 'FAIL',
                                     'error': 'Could not upload given data, please check it is conformant to our standard'})

        # Add information for location-history
        if ('location-history' in group_names):
            try:
                group_name = 'location-history'

                if json_file_lh is None:
                    results_info.append({'name': group_name, 'result': 'FAIL',
                                         'error': 'Could not upload given data, the realtive file is not present in the zip'})

                else:
                    location_history = json_file_lh['locations']
                    first = True
                    old_time = None
                    objects = []
                    for entry in location_history:
                        lat = entry['latitudeE7'] / 10000000.0
                        lng = entry['longitudeE7'] / 10000000.0
                        #time = float(entry['timestamp']) / 1000.0
                        #time = datetime.datetime.fromtimestamp(
                        #    time, tz=timezone.utc).strftime("%Y-%m-%d %H:%M:%S")

                        time_dt = datetime.fromisoformat(entry['timestamp'][:19])
                        time = time_dt.strftime("%Y-%m-%d %H:%M:%S")
                        #description = geolocator.reverse(
                        #    f'{lat}, {lng}').raw['address'].get('city', '')

                        # Add no more than 1 location per hour
                        if old_time is not None:
                            diff = time_dt - old_time
                            if diff.total_seconds() < 3600:
                                continue

                        old_time = time_dt

                        value = {
                            "visited-location": {
                                'latitude': lat,
                                'longitude': lng,
                                'description': "",
                                'time': time
                            }
                        }

                        data = {
                            "data_type": 'visited-location',
                            "value": value,
                            "group_name": 'location-history',
                            "metadata": 'metadata-visited-location',
                            "description": 'description-visited-location',
                        }

                        newDataSerializer = PersonalDataSerializer(data=data)
                        if (newDataSerializer.is_valid()):
                            pass
                            #newDataSerializer.save(owner=user)
                            pd = PersonalData(value=value,
                                data_type='visited-location',
                                group_name='location-history',
                                metadata= 'metadata-visited-location',
                                description= 'description-visited-location',
                                owner=user
                                )
                            objects.append(pd)

                        if first:
                            send_to_task_manager(serializer = newDataSerializer, username=self.request.user.username)
                        first = False
                    PersonalData.objects.bulk_create(objects)
                    results_info.append(
                            {'name': group_name, 'result': 'Created Objects: ' + str(len(objects)) })

            except Exception as e:
                results_info.append({'name': group_name, 'result': 'FAIL',
                                     'error': 'Could not upload given data, please check it is conformant to our standard. Error: ' + str(e)})

        return Response({"result": results_info}, status=status.HTTP_200_OK)
