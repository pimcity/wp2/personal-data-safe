# Packages imports
from django.core import serializers
from django.db.models.expressions import RawSQL
from django.conf import settings
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, AllowAny, IsAuthenticated
from dateutil import parser
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope


# Local files import
from ..models import PersonalData
from ..serializers import PersonalDataSerializer
from ..utils.validation_utils import validateUserData, isHistoricalData, getOrderingTypes, getDataType
from ..utils.query_utils import getPersonalDataQuery
from ..permissions import PersonalInformationPrivacyPermission
import requests

def send_to_task_manager(serializer, username):
    if settings.TASK_MANAGER_ENDPOINT is not None and settings.TASK_MANAGER_APIKEY is not None and settings.TASK_MANAGER_GROUP2TASK is not None:
        try:
            group_name = serializer["group_name"].value
            group2task = dict([e.split(":")for e in settings.TASK_MANAGER_GROUP2TASK.split(",")])
            if group_name in group2task:
                task = group2task[group_name]
                headers = {"X-API-Key": settings.TASK_MANAGER_APIKEY,
                           "Content-Type": "application/json",
                           "accept": "*/*"}
                response = requests.post( settings.TASK_MANAGER_ENDPOINT,
                                          json = {"taskId": task, "userId": username},
                                          headers=headers,
                                          timeout=3)
                if response.status_code != 200:
                    print("Error in the Task Manager, Return code:", response.status_code)
            else:
                print(f"Warning: group {group_name} cannot be handled by Task Manager")
        except Exception as  e:
            print("Error in sending Task to Task Manager:", e)
        print(f"Sent to Task Manager: Username: {username}, Group Name: {group_name}, task: {task}")

class PersonalDataAPIView(APIView):

    permission_classes = [PersonalInformationPrivacyPermission]

    def perform_create(self, serializer):
        '''
        Custom function to save the serialized data and adds the owner of the data as the request logged in user
        '''
        serializer.save(owner=self.request.user)
        send_to_task_manager(serializer = serializer, username=self.request.user.username)


    def get(self, request, format=None):
        '''
        This function gets all user personal data and returns them as response.

        Personal data can also be filtered with url queries.
        Queries:
            - "category": one of the macro-groups in the schema file (ex personal-information). Default is ALL.
            - "filter-start": starting interval among the selected data. Default is 0.
            - "filter-end": ending interval among the selected data. Default is 100.
            - "order-by": field type in the schema file that defines the ordering (ex. time or title in "browsing-history")
            - "order-type": 'desc' for descending order otherwise default is ascending
            - "time-start": starting time for the query in ISO 8601 format. Records are sorted by time
            - "time-end": end time for the query in ISO 8601 format. Records are sorted by time
        '''

        try:
            # Grabbing query params
            category = request.GET.get('category')
            filter_start = int(request.GET.get('filter-start', 0))
            filter_end = int(request.GET.get('filter-end', 100))
            order_by = request.GET.get('order-by', '')
            order_type = request.GET.get('order-type', '')
            
            time_start = request.GET.get('time-start')
            time_end = request.GET.get('time-end')
            if time_start:
                time_start = parser.parse(time_start)
                order_by = "time"
            if time_end:
                time_end = parser.parse(time_end)
                order_by = "time"

            # Normalizing filters value
            if (filter_start > filter_end):
                filter_end = filter_start + 100

            # Normalizing ordering char from desc to -
            if order_type == 'desc':
                order_type = '-'

        except Exception as e:
            error_message = "Could not set-up the query filters" + str(e)
            return Response({"error": error_message}, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = request.user
            if not category:
                objects_total, output_data = getPersonalDataQuery(
                    user=user, filter_start=filter_start, filter_end=filter_end)

            elif not order_by:
                objects_total, output_data = getPersonalDataQuery(
                    user=user, filter_start=filter_start, filter_end=filter_end, category=category)

            elif not time_start and not time_end:
                objects_total, output_data = getPersonalDataQuery(
                    user=user, filter_start=filter_start, filter_end=filter_end,
                    category=category, order_by=order_by, order_type=order_type)
                    
            else:
                objects_total, output_data = getPersonalDataQuery(
                    user=user, filter_start=filter_start, filter_end=filter_end,
                    category=category, time_start = time_start, time_end=time_end,
                    order_by="time")

            # Setting up the data for output
            output_data = {
                'query-details': {
                    'category': '',
                    'total-objects': objects_total,
                    'filter-start': filter_start,
                    'filter-end': filter_end
                },
                'data': output_data
            }

            return Response(output_data, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({"error": "Could not get the stored data from database: " + e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, format=None):
        '''
        This function adds a new data entry to the DB and returns the serialized created object as response.
            - Takes the POST request inputs
                - 'data-type'
                - 'value'
                - 'group-name'
                - 'metadata'
                - 'description'

            - Performs all check to ensure data integrity

            - Create a new DB entry by using the PersonalDataSerializer

            - Returns the serialized JSON data created as response
        '''

        # Gather data input
        try:
            data_type = request.data['type']
        except:
            return Response({"error": "Field 'type' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            value = {
                data_type: request.data['value']
            }
        except:
            return Response({"error": "Field 'value' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            group = request.data['group-name']
        except:
            return Response({"error": "Field 'group-name' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            metadata = request.data['metadata']
        except:
            metadata = ''

        try:
            description = request.data['description']
        except:
            description = ''        

        data = {
            "data_type": data_type,
            "value": value,
            "group_name": group,
            "metadata": metadata,
            "description": description
        }

        # Checking if the data inserted in the user request is valid with the schema
        isDataValid, error = validateUserData(data)

        if not isDataValid:
            return Response({"error": error}, status=status.HTTP_400_BAD_REQUEST)

        # Check if it is historical
        if isHistoricalData(request.user, data['group_name'], data['data_type']) == False:
            return Response({"error": "Data is not historical"}, status=409)

        # Setting up the data type from the schema file
        try:
            data['data_type'] = getDataType(group, data_type)
        except:
            return Response({"error": "Could not retrieve the data type from database"}, status=status.HTTP_400_BAD_REQUEST)

        # Serializing data
        newDataSerializer = PersonalDataSerializer(data=data)

        if (newDataSerializer.is_valid()):

            try:
                # Create new Personal data
                self.perform_create(newDataSerializer)
            except:
                return Response({"error": "An error occurred while creating new Personal Data entry"}, status=500)

            return Response(newDataSerializer.data, status=status.HTTP_201_CREATED)

        return Response({"error": newDataSerializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class PersonalDataAPIViewBatch(APIView):

    permission_classes = [PersonalInformationPrivacyPermission]

    def perform_create(self, serializer):
        '''
        Custom function to save the serialized data and adds the owner of the data as the request logged in user
        '''
        serializer.save(owner=self.request.user)


    def post(self, request, format=None):
        '''
        This function adds multiple data entry to the DB and returns the serialized created object as response.
            - Takes the POST request inputs as a list of:
                - 'data-type'
                - 'value'
                - 'group-name'
                - 'metadata'
                - 'description'

            - Performs all check to ensure data integrity

            - Create a new DB entry by using the PersonalDataSerializer

            - Returns the serialized JSON data created as response
        '''

        payload = request.data

        if not type(payload) == list:
            return Response({"error": "payload not a list"}, status=status.HTTP_400_BAD_REQUEST)

        first = True
        for pi in payload:
            # Gather data input
            try:
                data_type = pi['type']
            except:
                return Response({"error": "Field 'type' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

            try:
                value = {
                    data_type: pi['value']
                }
            except:
                return Response({"error": "Field 'value' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

            try:
                group = pi['group-name']
            except:
                return Response({"error": "Field 'group-name' could not be found"}, status=status.HTTP_400_BAD_REQUEST)

            try:
                metadata = pi['metadata']
            except:
                metadata = ''

            try:
                description = pi['description']
            except:
                description = ''        

            data = {
                "data_type": data_type,
                "value": value,
                "group_name": group,
                "metadata": metadata,
                "description": description
            }

            # Checking if the data inserted in the user request is valid with the schema
            isDataValid, error = validateUserData(data)

            if not isDataValid:
                return Response({"error": error}, status=status.HTTP_400_BAD_REQUEST)

            # Check if it is historical
            if isHistoricalData(request.user, data['group_name'], data['data_type']) == False:
                return Response({"error": "Data is not historical"}, status=409)

            # Setting up the data type from the schema file
            try:
                data['data_type'] = getDataType(group, data_type)
            except:
                return Response({"error": "Could not retrieve the data type from database"}, status=status.HTTP_400_BAD_REQUEST)

            # Serializing data
            newDataSerializer = PersonalDataSerializer(data=data)

            if (newDataSerializer.is_valid()):

                try:
                    # Create new Personal data
                    self.perform_create(newDataSerializer)
                except:
                    return Response({"error": "An error occurred while creating new Personal Data entry"}, status=500)
                if first:
                    send_to_task_manager(serializer = newDataSerializer, username=self.request.user.username)
                first = False

        return Response("OK", status=status.HTTP_201_CREATED)

