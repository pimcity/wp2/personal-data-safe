#
# As of 14/04/2021 this view is discontinued and should only be used if the main view is not working..
#

# Packages imports
from django.core import serializers
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

# Local files import
from ..models import PersonalData
from ..permissions import PersonalInformationPrivacyPermission
from ..serializers import PersonalDataSerializer
from ..utils.validation_utils import isMacroGroup


class PersonalDataCategoryAPIView(APIView):
    permission_classes = [PersonalInformationPrivacyPermission]

    def get(self, request, macro_group, format=None):
        '''
        This function returns the set of personal-data of a specific user defined by the 
        given macro_group of the request.
        The logic has the following steps:
            - check if the macro_group given is defined in the schema
            - get all personal-data of the user requesting having also the specified macrogroup
            - returns a serialized list of found personal-data

        '''
        if not isMacroGroup(macro_group):
            error_message = "The macro group in the format '../<macro_group>/' is not valid."
            return Response({"error": error_message}, status=status.HTTP_400_BAD_REQUEST)

        try:
            filter_start = request.GET.get('filter-start', 0)
            filter_end = request.GET.get('filter-end', 100)

        except Exception:
            error_message = "Could not set-up the query filters"
            return Response({"error": error_message}, status=status.HTTP_400_BAD_REQUEST)


        try:
            user = request.user
            personalDatas = PersonalData.objects.filter(owner=user, group_name=macro_group)[filter_start:filter_end]

            serializer = PersonalDataSerializer(personalDatas, many=True)

            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception:
            return Response({"error": "Could not get the stored data from database"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=status.HTTP_200_OK)
