from django.urls import path
from .views.personalDataViews import PersonalDataAPIView, PersonalDataAPIViewBatch
from .views.personalDataCategoryViews import PersonalDataCategoryAPIView
from .views.personalDataDetailViews import PersonalDataDetailAPIView
from .views.personalDataUtilityViews import apiOverview, getMacroGroups, getUserInsertions, getUserGraphsData
from .views.personalDataExtractionViews import PersonalDataExtractionAPIView


app_name = 'personal_data'

# Note: batch before <str:macro_group> to allow matching
urlpatterns = [
    path('personal-data/<int:pk>/', PersonalDataDetailAPIView.as_view(), name='personal_data_detail'),
    path('personal-data/zip-file/', PersonalDataExtractionAPIView.as_view(), name='personal_data_extraxction'),
    path('personal-data/batch/', PersonalDataAPIViewBatch.as_view(), name='personal_data_list_create_batch'),

    path('personal-data/<str:macro_group>/', PersonalDataCategoryAPIView.as_view(), name='personal_data_category'),
    path('personal-data/', PersonalDataAPIView.as_view(), name='personal_data_list_create'),
    path('utility/', apiOverview, name='api_overview'),
    path('utility/macro-groups', getMacroGroups, name='get_macro_groups'),
    path('utility/user-insertions', getUserInsertions, name='get_user_insertions'),
    path('utility/graph-data', getUserGraphsData, name='get_user_graph_data'),
]