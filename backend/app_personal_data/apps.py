# Packages import
from django.apps import AppConfig
from config import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import yaml

schema_yaml = None


class AppPersonalDataConfig(AppConfig):
    name = 'app_personal_data'

    # Function to load schema when system start
    def ready(self):
        '''
        Custom edit of base ready() function called when django system is innitiated.
        Takes the yaml file and loads it into a global schema_yaml variable, then it checks\i
            its validity.
        '''
        with open(settings.SCHEMA_PATH) as info:
            global schema_yaml
            schema_yaml = yaml.load(info, Loader=yaml.FullLoader)

        self.schemaValidation(schema_yaml)

    def schemaValidation(self, schema_yaml):
        '''
        This function is used to perform the controls on the structure of the schema.

        Errors are raised and prevent django from functioning when schema is not valid
        '''

        # Check if field 'content' is in the schema.yaml
        content = None
        if 'content' in schema_yaml:
            content = schema_yaml['content']
        else:
            # error
            raise ValidationError(
                _('Schema is not valid: content field has not been found'))

        # Check if the content field is not empty
        if not content:
            raise ValidationError(
                _('Schema is not valid: "content" field is empty'))

        # Loop cicle on content, i.e., on each group
        for group in content:

            # Check that all groups have a field 'group-name' and
            # a field 'types'
            group_name = None
            types = None
            if 'group-name' in group and 'types' in group:
                group_name = group['group-name']
                types = group['types']
            else:
                # error
                raise ValidationError(
                    _('Schema is not valid: either a "group_name" or a "types" field is missing'))

            # Check if the types field is not empty
            if not types:
                raise ValidationError(
                    _('Schema is not valid: "types" field is empty'))
            
            # Logic to assess the visualization-hint property, for now it only checks the map attribute
            if 'visualization-hint' in group:
                visualization_hint = group['visualization-hint']

                if not visualization_hint:
                    raise ValidationError(
                    _('Schema is not valid: "visualization-hint" present but not filled'))

                is_vh_valid = False

                # Check that at least one type has the properties needed for the visualization-hint
                for types_elem in types:
                    if types_elem['type'] == 'dict':
                        if {'latitude': 'float'} in types_elem['fields'] and {'longitude': 'float'} in types_elem['fields']:
                            is_vh_valid = True

                if not is_vh_valid:
                    raise ValidationError(
                    _('Schema is not valid: "visualization-hint: map" requires two fields {"latitude": "float"} and {"longitude": "float"}'))

            # Loop cicle on types, i.e., on each type
            for types_elem in types:

                # Check that all types have: - field 'name'
                #                            - field 'type'
                #                            - field 'historical'
                if 'name' in types_elem:
                    name = types_elem['name']
                else:
                    # error
                    ValidationError(
                        _('Schema is not valid: a "name" is missing from types'))

                if 'type' in types_elem:
                    data_type = types_elem['name']
                else:
                    # error
                    ValidationError(
                        _('Schema is not valid: a "type" is missing from types'))

                if 'historical' not in types_elem:
                    # error
                    ValidationError(
                        _('Schema is not valid: a "historical" is missing from types'))

                # Check that where a dict type is found, then it must contain some 'fields'
                if data_type == 'dict':

                    fields = None
                    if 'fields' in types_elem:
                        fields = types_elem['fields']
                    else:
                        # error
                        ValidationError(
                            _('Schema is not valid: (dict-type) "fields" field is missing'))

                    # Check if the types field is not empty
                    if not fields:
                        raise ValidationError(
                            _('Schema is not valid: "fields" field is empty'))
