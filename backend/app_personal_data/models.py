# Packages import
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.conf import settings
import datetime

# Local files
from .apps import schema_yaml


class PersonalData(models.Model):

    # value represents the effective value of the entry in the DB
    value = models.JSONField(db_index=True)

    # metadata represents the common name for the entry (e.g. first-name, visited-url)
    metadata = models.CharField(max_length=100, blank=True)

    # data_type is the type of the data, so that we can perform cast operations and type validation of the value
    data_type = models.CharField(max_length=100)

    # group_name is the macro group which the entry belongs to
    group_name = models.CharField(max_length=100, db_index=True)

    # description is a general description of the inserted information
    description = models.TextField(blank=True)

    # date in which the information is created
    created = models.DateTimeField(editable=False, default=timezone.now)

    # reference to the user
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE,
                              editable=False)

    def __str__(self):
        return self.metadata
