# Packages imports
from django.core import serializers
from django.db.models.expressions import RawSQL
from dateutil import parser
from datetime import datetime

# Local files import
from ..models import PersonalData
from ..serializers import PersonalDataSerializer
from .validation_utils import getOrderingTypes


def getPersonalDataQuery(user, filter_start=None, filter_end=None, category=None,
                         order_by=None, order_type='', time_start = None, time_end = None):
    # Initializing variables
    objects_total = 0

    # Creating different queries depending on data passed to function
    if not category:
        personalDatas = PersonalData.objects.filter(
            owner=user)
        objects_total = personalDatas.count()
        serializer = PersonalDataSerializer(
            personalDatas[filter_start:filter_end], many=True)
        output_data = serializer.data

    elif not order_by:
        personalDatas = PersonalData.objects.filter(
            owner=user, group_name=category)
        objects_total = personalDatas.count()
        serializer = PersonalDataSerializer(
            personalDatas[filter_start:filter_end], many=True)
        output_data = serializer.data

    #elif not time_start and not time_end:
    else:
        if not time_start: time_start = parser.parse("1970-01-01T00:00:00.0Z")
        if not time_end: time_end = datetime.now()
        # From the macro_group given we extract the types names to create the queries
        ordering_types = getOrderingTypes(category, order_by)
        order_type = f"{order_type}ordering"
        output_data = []

        if order_by == "time":
            # Ordering by date (field must have property time)
            for order_field in ordering_types:
                personalDatas = PersonalData.objects.filter(owner=user, group_name=category,
                        created__gte = time_start, created__lte = time_end).annotate(ordering=RawSQL(
                    "TO_DATE(value->%s->>%s, 'YYYY-MM-DD HH24:MI:SS')", (order_field, order_by))).order_by(order_type)

                objects_total = personalDatas.count()
                serializer = PersonalDataSerializer(
                    personalDatas[filter_start:filter_end], many=True)
                output_data = output_data + serializer.data

        if order_by == "title":
            # Ordering by title (field must have property 'title')
            for order_field in ordering_types:
                personalDatas = PersonalData.objects.filter(owner=user, group_name=category,
                    created__gte = time_start, created__lte = time_end).annotate(ordering=RawSQL(
                    "value->%s->>%s", (order_field, order_by))).order_by(order_type)

                objects_total = personalDatas.count()
                serializer = PersonalDataSerializer(
                    personalDatas[filter_start:filter_end], many=True)
                output_data = output_data + serializer.data
                
    # else: # Start time or end time
    #     if not time_start: time_start = parser.parse("1970-01-01T00:00:00.0Z")
    #     if not time_end: time_end = datetime.now()

    #     personalDatas = PersonalData.objects.filter( owner=user, group_name=category,
    #                                                  created__gte = time_start, created__lte = time_end)
            
    #     objects_total = personalDatas.count()
    #     serializer = PersonalDataSerializer(
    #         personalDatas[filter_start:filter_end], many=True)
    #     output_data = serializer.data

    return [objects_total, output_data]
