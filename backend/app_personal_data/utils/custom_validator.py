from django.contrib.auth import get_user_model
from oauth2_provider.oauth2_validators import OAuth2Validator
from oauth2_provider.settings import oauth2_settings
from oauth2_provider.models import get_access_token_model

from datetime import datetime
import http.client
import logging
import jwt
import base64
import requests
import json

log = logging.getLogger("oauth2_provider")
UserModel = get_user_model()
AccessToken = get_access_token_model()

class CustomOAuth2Validator(OAuth2Validator):

    def validate_bearer_token(self, token, scopes, request):

        # Skip if not token
        if not token:
            return False
  
        # Decode Token
        jwt_token = jwt.decode(token, options={"verify_signature": False})
        
        # If key is available can use:
        #jwt.decode(token, key="-----BEGIN PUBLIC KEY-----\n" + public_key + '\n-----END PUBLIC KEY-----')
        # Copy the key from KeyCloak
        
        if not "sub" in jwt_token:
            log.debug("No Subject in the Token")
            return False
            
        # Validate token
        introspection_url = oauth2_settings.RESOURCE_SERVER_INTROSPECTION_URL
        introspection_token = oauth2_settings.RESOURCE_SERVER_AUTH_TOKEN
        introspection_credentials = oauth2_settings.RESOURCE_SERVER_INTROSPECTION_CREDENTIALS
        scopes_required = set(oauth2_settings.SCOPES.keys())
                
        headers = None
        if introspection_token:
            headers = {"Authorization": "Bearer {}".format(introspection_token)}
        elif introspection_credentials:
            client_id = introspection_credentials[0].encode("utf-8")
            client_secret = introspection_credentials[1].encode("utf-8")
            basic_auth = base64.b64encode(client_id + b":" + client_secret)
            headers = {"Authorization": "Basic {}".format(basic_auth.decode("utf-8"))}
        else:
            log.debug("There is a Token but no Credentials to verify.")
            return False

        try:
            response = requests.post(introspection_url, data={"token": token}, headers=headers)
        except requests.exceptions.RequestException:
            log.exception("Introspection: Failed POST to %r in token lookup", introspection_url)
            return False
            
        if response.status_code != http.client.OK:
            log.exception(
                "Introspection: Failed to get a valid response "
                "from authentication server. Status code: {}, "
                "Reason: {}.".format(response.status_code, response.reason)
            )
            return False
        try:
            content = response.json()
        except ValueError:
            log.exception("Introspection: Failed to parse response as json")
            return False

        if "active" in content and content["active"] is True:
        
            # Get information
            username = jwt_token["sub"]
            
            user, _created = UserModel.objects.get_or_create(
                        **{UserModel.USERNAME_FIELD: username}
                    )
                    
            request.user = user
            request.scopes = set(jwt_token["scope"].split())
            request.client = jwt_token["azp"]
            
            # Check sets:
            if not scopes_required.issubset(request.scopes):
                log.debug("Insufficient scopes in the Token")
                return False
            
            # Create the token for REST
            access_token, _created = AccessToken.objects.update_or_create(
                    token=jwt_token["jti"],
                    defaults={
                        "user": request.user,
                        "expires": datetime.utcfromtimestamp(jwt_token["exp"]),
                        "scope": jwt_token["scope"],
                        "application": None,
                    })
                    
            request.access_token = access_token 
                
            #print ("scopes", request.scopes )
            #print ("user", request.user )
            #print ("client", request.client)
            
            return True
            
        else:
            log.debug("Token not active")
            return False
