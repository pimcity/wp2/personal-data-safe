'''
File containing utility functions
'''
# Packages import
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
import datetime
from dateutil import parser as dateparser

# Local files import
from ..apps import schema_yaml
from ..models import PersonalData


def validateUserData(data):
    '''
    Custom function to check if the data inputted by the user is conform with our model (schema.yaml)

    Each content schema is:
        - content:
            - group-name
            - types:
                - name
                - historical
                - type
                - fields (optional only if type=='dict')

    Input: (dict) with following mandatory fields: 'group-name', 'data-type', 'value'

    Output: (touple) can either be
                        - (True, None) if the validation is successful
                        - (False, error) where error is a string describing the error
                                            in case of unsuccessful validation
    '''

    # Trying to match the given group_name with the schema's group_name
    found_group = {}
    for group in schema_yaml['content']:
        if group['group-name'] == data['group_name']:
            found_group = group

    # Checking if a group has been found
    if not found_group:
        error_message = f"The group-name '{data['group_name']}' is not in the schema"
        return (False, error_message)

    # Trying to match the given data_type with the group's data_type
    found_data_type = {}
    for data_type in found_group['types']:
        if data_type['name'] == data['data_type']:
            found_data_type = data_type

    # Checking if a data_type has been found
    if not found_data_type:
        error_message = f"The data-type '{data['data_type']}' is not in the schema"
        return (False, error_message)

    # Validate the data
    found_data_fields = None

    if found_data_type['type'] == 'dict':
        found_data_fields = found_data_type['fields']

    # next(iter()) function returns the first element of a dict (Python 3.6 at least)
    index = next(iter(data['value']))
    data_value = data['value'][index]
    data_type = found_data_type['type']

    is_valid, error_message = validateInformationType(data_value,
                                                      data_type,
                                                      found_data_fields)

    if not is_valid:
        return (is_valid, error_message)

    if not data['value']:
        error_message = f"The value has not been found"
        return (False, error_message)

    return (True, None)


def validateInformationType(data, data_type, fields):
    '''
    This function check the consistency of the information type
    '''
    data_value = None

    # Extract the value if data_type is not dict but the data is a dict object
    # Example data_type = int data = {'int': 24}
    if (data_type != 'dict'):
        if (type(data) == dict):
            data_value = list(data.values())[0]
        else:
            data_value = data

    # the types that are supported are int, float, date, boolean, dict, string
    if (data_type == 'int'):
        try:
            # Cast to int
            data_value = int(data_value)
        except:
            return (False, str(data_value) + ' is not an int value')

    elif (data_type == 'float'):
        try:
            # Cast to float
            data_value = float(data_value)
        except:
            return (False, str(data_value) + ' is not an float value')

    elif (data_type == 'date'):
        # Cast to date for validity and back to string
        try:
            if type(data_value) == str:
                data_value = try_parsing_date(data_value)

            if type(data_value) != datetime.datetime:
                error_message = str(
                    data_value) + " is not an date in the format 'YYYY-mm-dd HH:MM:SS' "
                return (False, error_message)

            data_value = data_value.strftime("%Y-%m-%d %H:%M:%S")

        except:
            error_message = str(
                data_value) + " is not an date in the format 'YYYY-mm-dd HH:MM:SS' "
            return (False, error_message)

    elif (data_type == 'boolean'):
        # Cast to boolean
        try:
            data_value = str2Bool(data_value)
        except:
            return (False, str(data_value) + ' is not an boolean')

    elif (data_type == 'string'):
        # For string there is nothing to do, since the data is already a string
        pass

    elif (data_type == 'dict'):
        # Cast to int
        try:
            dict_ = data
            # enforce type of dictionary -> I have to loop over the element in the dictionary
            for item in fields:
                for key in item:
                    if (key not in dict_.keys()):
                        return (False, f"The dict key '{key}' missing to match the schema")

                    # For each element of the dict I repeat the type validation
                    is_dict_valid, error = validateInformationType(dict_[key],
                                                                   item[key], None)

                    if not is_dict_valid:
                        return (False, error)

        except ValueError as ve:
            return (False, str(ve))
    else:
        return (False, 'Type inserted is not supported')

    # Validation successful
    if type(data) == dict and data_type != "dict":
        data[data_type] = data_value
        data.pop('', None)
    elif data_type == "dict":
        data = dict_
    else:
        data = data_value

    return (True, None)


def try_parsing_date(text):
    '''
    Function for date parsing given an input string

    TODO It should be added the functionality to recognize different date formats
    '''
    try:
        found_time = timezone.make_aware(
            datetime.datetime.strptime(text, '%Y-%m-%d %H:%M:%S'))
        return found_time

    except:
        try:
            found_time = timezone.make_aware(dateparser.isoparse(text))
            return found_time
        except:
            raise ValueError('No valid date format found')


def str2Bool(data):
    '''
    Function for converting a string to boolean value
    '''
    try:
        data = data.lower()
    except:
        raise ValueError
    if data == 'true':
        return True
    elif data == 'false':
        return False
    else:
        raise ValueError


def isHistoricalData(username, groupName, nameVal):
    '''
    This function is used to know if a value is historical or not, so to understand if the entry can be inserted in the P-DS

    Input: - (str) username: the unique username string
           - (str) groupName: the group name identified as in the schema.yaml file
           - (str) nameVal: the name of the type category as in the schema.yaml file

    Output: (bool)  - True: If the data is historical can be inserted multiple time
                    - True: If the data is not historical and it's NOT present in the DB, it can be inserted
                    - False: If the data is not historical and it's present in the DB, it can not be inserted
    '''

    try:
        content = schema_yaml['content']

        for group in content:
            group_name = group['group-name']

            if(group_name == groupName):

                for types_elem in group['types']:

                    name = types_elem['name']

                    if name == nameVal:
                        historical = types_elem['historical']

                        user = get_user_model().objects.get(username=username)
                        info_querySet = PersonalData.objects.filter(
                            owner__id=user.id, metadata=nameVal)

                        if(historical == True):
                            return True

                        if(info_querySet.count() == 0):
                            return True

                        return False

        return False

    except Exception:
        return False


def isEditableData(username, groupName, nameVal, object_id):
    '''
    This function is used to know if a value is historical or not, so to understand if the entry can be inserted in the P-DS

    Input: - (str) username: the unique username string
           - (str) groupName: the group name identified as in the schema.yaml file
           - (str) nameVal: the name of the type category as in the schema.yaml file

    Output: (bool)  - True: If the data is editable in the DB
                    - False: If data is NOT present in the DB or if cannot be updated
                    - False: Otherwise
    '''

    try:
        content = schema_yaml['content']

        for group in content:
            group_name = group['group-name']

            if(group_name == groupName):

                for types_elem in group['types']:

                    name = types_elem['name']

                    if name == nameVal:
                        historical = types_elem['historical']

                        user = get_user_model().objects.get(username=username)
                        info_querySet = PersonalData.objects.filter(
                            pk=object_id)

                        if(info_querySet.count() == 0):
                            return False

                        return group['user-update']

        return False

    except Exception:
        return False


def isUserInsertionData(username, groupName, nameVal):
    '''
    This function is used to know if a value is historical or not, so to understand if the entry can be inserted in the P-DS

    Input: - (str) username: the unique username string
           - (str) groupName: the group name identified as in the schema.yaml file
           - (str) nameVal: the name of the type category as in the schema.yaml file

    Output: (bool)  - True: If the data can be inserted manually by the user when no entries are in DB
    Output: (bool)  - True: If the data can be inserted manually by the user when is historical
                    - False: If cannot be manually inserted or if it is historical
                    - False: Otherwise
    '''

    try:
        content = schema_yaml['content']

        for group in content:
            group_name = group['group-name']

            if(group_name == groupName):

                for types_elem in group['types']:

                    name = types_elem['name']

                    if name == nameVal:

                        user = get_user_model().objects.get(username=username)
                        info_querySet = PersonalData.objects.filter(
                            owner__id=user.id, metadata=nameVal)

                        if(info_querySet.count() == 0):
                            # When no instance exists in DB check if user can insert
                            return group['user-insertion']

                        # If instance exists in DB checks if user can insert AND if it is historical
                        return types_elem['historical'] and group['user-insertion']

        return False

    except Exception:
        return False


def isMacroGroup(macro_group):
    '''
    This function checks if the string macro_group passed is contained in the schema.yaml file

    Input: (str) macro_group: string identifying a macro_group

    Output: (bool) True if the macro_group exists in the schema
    '''

    content = schema_yaml['content']

    for group in content:
        group_name = group['group-name']

        if(group_name == macro_group):
            return True

    return False


def getOrderingTypes(macro_group, order_field):
    '''
    This function checks if the macro_group passed is can be ordered.

    Input:  - (str) macro_group: string identifying a macro_group
            - (str) order_field: field by which ordering is requested (it is the key of the fields dict in the schema file)

    Output: - (list) list of all the ordering_types names that have order_field as possible ordering
    '''
    content = schema_yaml['content']
    ordering_fields = []

    for group in content:
        group_name = group['group-name']

        if(group_name == macro_group):

            for types_elem in group['types']:

                name = types_elem['name']

                if 'fields' in types_elem:

                    for field in types_elem['fields']:

                        if list(field.keys())[0] == order_field:
                            ordering_fields.append(name)

    return ordering_fields


def getDataType(macro_group, sub_group):
    try:
        macro_group = next(
            item for item in schema_yaml['content'] if item["group-name"] == macro_group)
        sub_group = next(
            item for item in macro_group['types'] if item['name'] == sub_group)
        return(sub_group['type'])
    except:
        return 'undefined'
