import os
import django

# Next two lines are needed for imports, they need to be on top
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from app_personal_data.models import PersonalData
import sys

if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception('Missing macro-group name. ex [location-history]')

    user_confirmation_text = (
        f"""
    Are you sure you want to delete everything from {sys.argv[1]}?
    [1] - Confirm
    [0] - Cancel
    """)
    user_confirmation = input(user_confirmation_text)

    try:
        category = sys.argv[1]
        choice = int(user_confirmation)
    except Exception:
        raise Exception(f"[{user_confirmation}] is not a valid choice")

    if choice == 0:
        print("Operation cancelled..")
    elif choice != 1:
        print(f"[{choice}] is not a valid choice")
    else:
        try:
            print(f"Deleting all entries in macro-group: {category}")
            PersonalData.objects.filter(group_name=category).delete()
            print(f"Operation successfull")

        except Exception:
            raise Exception('Operation failed...')
