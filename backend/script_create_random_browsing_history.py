'''
This script creates a set of random visited locations for the user inserted as argument.

Usage: create_random_visisted_location.py [username] [amount]
'''
import os
import django

# Next two lines are needed for imports, they need to be on top
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from django.contrib.auth import get_user_model
from app_personal_data.serializers import PersonalDataSerializer
from app_personal_data.models import PersonalData
import time
import json
import random
from datetime import timedelta
from datetime import datetime
import sys

urls_list = [
    'http://www.youtube.com',
    'http://www.facebook.com',
    'http://www.baidu.com',
    'http://www.yahoo.com',
    'http://www.amazon.com',
    'http://www.wikipedia.org',
    'http://www.qq.com',
    'http://www.google.co.in',
    'http://www.twitter.com',
    'http://www.live.com',
    'http://www.taobao.com',
    'http://www.bing.com',
    'http://www.instagram.com',
    'http://www.weibo.com',
    'http://www.sina.com.cn',
    'http://www.linkedin.com',
    'http://www.yahoo.co.jp',
    'http://www.msn.com',
    'http://www.vk.com',
    'http://www.google.de',
    'http://www.yandex.ru',
    'http://www.hao123.com',
    'http://www.google.co.uk',
    'http://www.reddit.com',
    'http://www.ebay.com',
    'http://www.google.fr',
    'http://www.t.co',
    'http://www.tmall.com',
    'http://www.google.com.br',
    'http://www.360.cn',
    'http://www.sohu.com',
    'http://www.amazon.co.jp',
    'http://www.pinterest.com',
    'http://www.netflix.com',
    'http://www.google.it',
    'http://www.google.ru',
    'http://www.microsoft.com',
    'http://www.google.es',
    'http://www.wordpress.com',
    'http://www.gmw.cn',
    'http://www.tumblr.com',
    'http://www.paypal.com',
    'http://www.blogspot.com',
    'http://www.imgur.com',
    'http://www.stackoverflow.com',
    'http://www.aliexpress.com',
    'http://www.naver.com',
    'http://www.ok.ru',
    'http://www.apple.com',
    'http://www.github.com',
    'http://www.chinadaily.com.cn',
    'http://www.imdb.com',
    'http://www.google.co.kr',
    'http://www.fc2.com',
    'http://www.jd.com',
    'http://www.blogger.com',
    'http://www.163.com',
    'http://www.google.ca',
    'http://www.whatsapp.com',
    'http://www.amazon.in',
    'http://www.office.com',
    'http://www.tianya.cn',
    'http://www.google.co.id',
    'http://www.youku.com',
    'http://www.rakuten.co.jp',
    'http://www.craigslist.org',
    'http://www.amazon.de',
    'http://www.nicovideo.jp',
    'http://www.google.pl',
    'http://www.soso.com',
    'http://www.bilibili.com',
    'http://www.dropbox.com',
    'http://www.xinhuanet.com',
    'http://www.outbrain.com',
    'http://www.pixnet.net',
    'http://www.alibaba.com',
    'http://www.alipay.com',
    'http://www.microsoftonline.com',
    'http://www.booking.com',
    'http://www.googleusercontent.com',
    'http://www.google.com.au',
    'http://www.popads.net',
    'http://www.cntv.cn',
    'http://www.zhihu.com',
    'http://www.amazon.co.uk',
    'http://www.diply.com',
    'http://www.coccoc.com',
    'http://www.cnn.com',
    'http://www.bbc.co.uk',
    'http://www.twitch.tv',
    'http://www.wikia.com',
    'http://www.google.co.th',
    'http://www.go.com',
    'http://www.google.com.ph',
    'http://www.doubleclick.net',
    'http://www.onet.pl',
    'http://www.googleadservices.com',
    'http://www.accuweather.com',
    'http://www.googleweblight.com',
    'http://www.answers.yahoo.com',
]

def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception('Two arguments needed. In order [username] [amount]')

    try:
        username = sys.argv[1]
        amount = int(sys.argv[2])
    except Exception:
        raise Exception('Could process given arguments..')

    date_lower_bound = datetime.strptime(
        '2010-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')
    date_upper_bound = datetime.strptime(
        '2020-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')

    for _ in range(amount):
        try:
            url = random.choice(urls_list)
            page_title = url[11:].split('.', 1)[0].title()
            
            value = {
                "visited-url": {
                    'url': url,
                    'title': page_title,
                    'time': random_date(date_lower_bound, date_upper_bound).strftime("%Y-%m-%d %H:%M:%S"),
                }
            }

            data = {
                "data_type": 'visited-url',
                "value": value,
                "group_name": 'browsing-history',
                "metadata": 'metadata-visited-url',
                "description": 'description-visited-url'
            }

            newDataSerializer = PersonalDataSerializer(data=data)
            user = get_user_model().objects.get(username=username)
            if (newDataSerializer.is_valid()):
                newDataSerializer.save(owner=user)

            print(newDataSerializer.data)

        except Exception:
            raise Exception('Something went wrong...')
