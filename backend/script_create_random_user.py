'''
This script creates a set of random visited locations for the user inserted as argument.

Usage: create_random_visisted_location.py [username] [amount]
'''
import os
import django

# Next two lines are needed for imports, they need to be on top
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from django.contrib.auth import get_user_model
from app_personal_data.serializers import PersonalDataSerializer
from app_personal_data.models import PersonalData
from app_users.serializers import RegisterUserSerializer
from app_users.models import User
import time
import json
import random
from datetime import timedelta
from datetime import datetime
import sys

names = [
    'Liam',
    'Olivia',
	'Noah',
    'Emma',
	'Oliver',
    'Ava',
	'William',
    'Sophia',
	'Elijah',
    'Isabella',
	'James',
    'Charlotte',
	'Benjamin',
    'Amelia',
	'Lucas',
    'Mia',
	'Mason',
    'Harper',
	'Ethan',
    'Evelyn',
]

last_names = [
    'Smith',
    'Johnson',
    'Williams',
    'Brown',
    'Jones',
    'Miller',
    'Davis',
    'Garcia',
    'Rodriguez',
    'Wilson',
    'Martinez',
    'Anderson',
    'Taylor',
    'Thomas',
    'Hernandez',
    'Moore',
    'Martin',
    'Jackson',
    'Thompson',
    'White',
    'Lopez',
    'Lee',
    'Gonzalez',
    'Harris',
    'Clark',
    'Lewis',
    'Robinson',
    'Walker',
    'Perez',
    'Hal',
]

def random_username():
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    name = random.choice(names).lower()
    last_name = random.choice(last_names).lower()
    return name + '_' + last_name

def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception('Two arguments needed. In order [amount]')

    try:
        amount = int(sys.argv[1])
    except Exception:
        raise Exception('Could process given arguments..')

    date_lower_bound = datetime.strptime(
        '1970-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')
    date_upper_bound = datetime.strptime(
        '2000-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')

    username = random_username()

    for _ in range(amount):
        # try:
        reg_serializer = RegisterUserSerializer(data={
            'username': username,
            'password': 'test_password'
        })

        if reg_serializer.is_valid():
            try:
                new_user = reg_serializer.save()
            except Exception:
                raise Exception('Something went wrong while creating new user')

        print("New User")
        print(reg_serializer.data)

        
        value = {
            "birth-data": random_date(date_lower_bound, date_upper_bound).strftime("%Y-%m-%d")
        }

        data = {
            "data_type": 'birth-data',
            "value": value,
            "group_name": 'personal-information',
            "metadata": 'metadata-birth-data',
            "description": 'description-birth-data'
        }

        newDataSerializer = PersonalDataSerializer(data=data)
        user = get_user_model().objects.get(username=username)
        if (newDataSerializer.is_valid()):
            newDataSerializer.save(owner=user)

        print(newDataSerializer.data)

        # except Exception:
        # raise Exception('Something went wrong...')
