from django.contrib import admin
from .models import DatabuyerRequest


class DatabuyerRequestCustomAdmin(admin.ModelAdmin):
    '''
    Custom Admin page config to better display information in the admin page
    '''

    list_display = ('id', 'databuyer_id', 'result', 'request_date')
    readonly_fields=('id', 'databuyer_id', 'jwt_token', 'result', 'request_date')


admin.site.register(DatabuyerRequest, DatabuyerRequestCustomAdmin)

