# Packages imports
from django.core import serializers
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
import inspect
import os.path
from datetime import datetime
import time
from dateutil import parser
import jwt

from oauth2_provider.settings import oauth2_settings
from config import settings
import http.client
import logging
import base64
import requests
import json

# Local files import
from app_personal_data.models import PersonalData
from app_users.models import User
from app_personal_data.serializers import PersonalDataSerializer
from .models import DatabuyerRequest

log = logging.getLogger("oauth2_provider")

class DatabuyersAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        '''
        This function return information about users provided the client credentials
        '''

        users = request.data.get("users", [])          
        data_group = request.data.get("data-group", None)
        data_type = request.data.get("data-type", None)
        
        time_start = request.GET.get('time-start')
        time_end = request.GET.get('time-end')
        access_token = request.META['HTTP_AUTHORIZATION'].split(" ")[-1]
        
        if time_start:
            time_start = parser.parse(time_start)
            order_by = "time"
        if time_end:
            time_end = parser.parse(time_end)
            order_by = "time"
        if not time_start: time_start = parser.parse("1970-01-01T00:00:00.0Z")
        if not time_end: time_end = datetime.now()

        if not validate_bearer_token(access_token):
            return Response({"error": "Unauthorized"}, status=status.HTTP_400_BAD_REQUEST)
            
        output_data = {}
        for user in users:
            user_obj = User.objects.filter(username = user).first()
            #print (user_id.id)
            
            personalDatas = PersonalData.objects.filter( owner=user_obj,
                                                         group_name=data_group,
                                                         data_type=data_type, 
                                                         created__gte = time_start,
                                                         created__lte = time_end)\
                                                         .order_by("created")

            serializer = PersonalDataSerializer( personalDatas, many=True)
            output_data[user] = serializer.data


        if len (users) > 0:
            # If everything is successfull then we add a new entry in the databuyers db
            db_request = DatabuyerRequest(
                databuyer_id=0, jwt_token=access_token, result=True)
            db_request.save()
            
        #return Response({"error": f"{e}"}, status=status.HTTP_400_BAD_REQUEST)

        return Response(output_data, status=status.HTTP_200_OK)
        
        
def validate_bearer_token(token):

        if not token:
            return False
  
        # Decode Token
        jwt_token = jwt.decode(token, options={"verify_signature": False})
        
        # If key is available can use:
        #jwt.decode(token, key="-----BEGIN PUBLIC KEY-----\n" + public_key + '\n-----END PUBLIC KEY-----')
        # Copy the key from KeyCloak
        
        if not "sub" in jwt_token:
            return False
            
        # Validate token
        introspection_url = oauth2_settings.RESOURCE_SERVER_INTROSPECTION_URL
        introspection_token = oauth2_settings.RESOURCE_SERVER_AUTH_TOKEN
        introspection_credentials = oauth2_settings.RESOURCE_SERVER_INTROSPECTION_CREDENTIALS
        scopes_required = set(settings.DATA_BUYER_OAUTH2_SCOPE.keys())
                
        headers = None
        if introspection_token:
            headers = {"Authorization": "Bearer {}".format(introspection_token)}
        elif introspection_credentials:
            client_id = introspection_credentials[0].encode("utf-8")
            client_secret = introspection_credentials[1].encode("utf-8")
            basic_auth = base64.b64encode(client_id + b":" + client_secret)
            headers = {"Authorization": "Basic {}".format(basic_auth.decode("utf-8"))}
        else:
            log.debug("There is a Token but no Credentials to verify.")
            return False

        try:
            response = requests.post(introspection_url, data={"token": token}, headers=headers)
        except requests.exceptions.RequestException:
            log.exception("Introspection: Failed POST to %r in token lookup", introspection_url)
            return False
            
        if response.status_code != http.client.OK:
            log.exception(
                "Introspection: Failed to get a valid response "
                "from authentication server. Status code: {}, "
                "Reason: {}.".format(response.status_code, response.reason)
            )
            return False
        try:
            content = response.json()
        except ValueError:
            log.exception("Introspection: Failed to parse response as json")
            return False

        if "active" in content and content["active"] is True:
        
            scopes = set(jwt_token["scope"].split())
            client = jwt_token["azp"]
            
            # Check sets:
            if not scopes_required.issubset(scopes):
                log.debug("Insufficient scopes in the Token.")
                return False
            
            return True
            
        else:
            return False
