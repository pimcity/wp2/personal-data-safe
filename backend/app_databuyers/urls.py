from django.urls import path
from .views import DatabuyersAPIView

app_name = 'databuyers'

urlpatterns = [
    path('get-data/', DatabuyersAPIView.as_view(), name='databuyers-api'),
]