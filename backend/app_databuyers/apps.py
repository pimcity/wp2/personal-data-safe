from django.apps import AppConfig


class AppDatabuyersConfig(AppConfig):
    name = 'app_databuyers'
