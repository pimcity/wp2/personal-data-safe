from django.db import models
from django.utils import timezone

class DatabuyerRequest(models.Model):

    # The ID of the databuyer performing the request
    databuyer_id = models.IntegerField(editable=False, blank=False)

    # The token used to perform the request (343 bytes is the max size of a jwt )
    jwt_token = models.CharField(max_length=2048, editable=False, blank=False)

    # boolean value to determine if the results was delivered or not
    result = models.BooleanField(editable=False, blank=False)

    # The date in which the information is created
    request_date = models.DateTimeField(editable=False, default=timezone.now)

    def __str__(self):
        return f"{self.request_date} - {self.databuyer_id} - {self.result}"

