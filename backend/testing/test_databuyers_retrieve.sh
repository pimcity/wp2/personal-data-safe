
# Obtain Token with:
curl --request POST \
  --url 'https://easypims.pimcity-h2020.eu/identity/auth/realms/pimcity/protocol/openid-connect/token' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data grant_type=client_credentials \
  --data client_id=pds \
  --data client_secret=SECRET

# Use it:
# Change Token With a New One
curl -X 'POST' \
  'https://easypims.pimcity-h2020.eu/pds/api/data-buyers/get-data/' \
  -H 'accept: */*' \
  -H 'Authorization: Bearer <TOKEN>' \
  -H 'Content-Type: application/json' \
  -d '{
  "users": [
    "tony",
    "lucas_martinez",
    "ba50c93a-ff3e-4e7b-96be-6653ccdd3db9"
  ],
  "data-group": "personal-information",
  "data-type": "age"
}'

