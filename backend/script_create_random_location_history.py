'''
This script creates a set of random visited locations for the user inserted as argument.

Usage: create_random_visisted_location.py [username] [amount]
'''
import os
import django

# Next two lines are needed for imports, they need to be on top
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from django.contrib.auth import get_user_model
from app_personal_data.serializers import PersonalDataSerializer
from app_personal_data.models import PersonalData
import pycristoforo as pyc
import time
import json
import random
from datetime import timedelta
from datetime import datetime
import sys



def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception('Two arguments needed. In order [username] [amount]')

    try:
        username = sys.argv[1]
        amount = int(sys.argv[2])
    except Exception:
        raise Exception('Could process given arguments..')

    country = pyc.get_shape("Italy")
    points = pyc.geoloc_generation(country, 500, "Italy")

    date_lower_bound = datetime.strptime(
        '2010-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')
    date_upper_bound = datetime.strptime(
        '2020-1-1 12:00:00', '%Y-%m-%d %H:%M:%S')

    for _ in range(amount):
        try:
            value = {
                "visited-location": {
                    'latitude': random.choice(points)['geometry']['coordinates'][1],
                    'longitude': random.choice(points)['geometry']['coordinates'][0],
                    'time': random_date(date_lower_bound, date_upper_bound).strftime("%Y-%m-%d %H:%M:%S"),
                    'description': 'example'
                }
            }

            data = {
                "data_type": 'visited-location',
                "value": value,
                "group_name": 'location-history',
                "metadata": 'metadata-visited-location',
                "description": 'description-visited-location'
            }

            newDataSerializer = PersonalDataSerializer(data=data)
            user = get_user_model().objects.get(username=username)
            if (newDataSerializer.is_valid()):
                newDataSerializer.save(owner=user)

            print(newDataSerializer.data)

        except Exception:
            raise Exception('Something went wrong...')
