from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from datetime import timedelta
from django.conf import settings
from .models import User


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    '''
    This custom class is used to add additional information when a token request is successfully sent
    The method "validate" is taken from the already existing method in TokenObtainPairSerializer
    '''

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['username'] = user.username
        return token

    def validate(self, attrs):
        # The default result (access/refresh tokens)
        data = super(CustomTokenObtainPairSerializer, self).validate(attrs)

        # Custom data you want to include
        data.update({'username': self.user.username})
        data.update({'user_id': self.user.id})
        data.update({'access_lifetime': settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME']})
        data.update({'refresh_lifetime': settings.SIMPLE_JWT['REFRESH_TOKEN_LIFETIME']})

        # and everything else you want to send in the response
        return data


class CustomTokenRefreshSerializer(TokenRefreshSerializer):
    '''
    This custom class is used to add additional information when a token refresh request is successfully sent
    The method "validate" is taken from the already existing method in TokenRefreshSerializer
    '''

    def validate(self, attrs):
        # The default result (access/refresh tokens)
        data = super(CustomTokenRefreshSerializer, self).validate(attrs)

        # Custom data you want to include
        data.update({'access_lifetime': settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME']})

        # and everything else you want to send in the response
        return data


class RegisterUserSerializer(serializers.ModelSerializer):
    '''
    This class deals with Registration of new users in the system
    '''

    class Meta():
        model = User
        fields = ('username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        instance = self.Meta.model(**validated_data)

        if password is not None:
            instance.set_password(password)

        instance.save()
        return instance
