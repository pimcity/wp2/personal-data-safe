# Packages
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework.permissions import IsAuthenticated

import time
# Custom files
from app_personal_data.permissions import PersonalInformationPrivacyPermission
from .models import User
from .serializers import RegisterUserSerializer, CustomTokenObtainPairSerializer, CustomTokenRefreshSerializer


class TokenFromSession(APIView):
    '''
    Obtain a token when session-authenticated.
    '''

    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):

        user = User.objects.get(pk=request.user.id)

        access_token = AccessToken.for_user(user)
        refresh_token = RefreshToken.for_user(user)

        res_data = {
            'user_id': user.id,
            "username": user.username,
            'access': str(access_token),
            'access_lifetime': access_token.lifetime.total_seconds(),
            'refresh': str(refresh_token),
            'refresh_lifetime': refresh_token.lifetime.total_seconds(),
        }

        return Response(res_data, status=status.HTTP_200_OK)
        

class CustomTokenObtainPairView(TokenObtainPairView):
    '''
    Custom view that adds the custom serializer for additional information upon token request
    '''

    # Replace the serializer with your custom
    serializer_class = CustomTokenObtainPairSerializer


class CustomTokenRefreshView(TokenRefreshView):
    '''
    Custom view that adds the custom serializer for additional information upon token refresh request
    '''

    # Replace the serializer with your custom
    serializer_class = CustomTokenRefreshSerializer


class BlacklistTokenView(APIView):
    '''
    Views used to deal with blacklisted tokens upon logout or other activities
    '''

    permission_classes = [AllowAny]

    def post(self, request):
        '''
        This function blacklists the given token
        '''
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": "Error while blacklisting token"}, status=status.HTTP_400_BAD_REQUEST)


class UserCreate(APIView):
    '''
    Rest API class that deals with user registration
    '''

    # Registration path should be allowed to any user
    permission_classes = [IsAdminUser]

    def post(self, request):
        '''
        POST request for creating a new user
        '''

        reg_serializer = RegisterUserSerializer(data=request.data)

        if reg_serializer.is_valid():
            try:
                new_user = reg_serializer.save()
                return Response({"res": "User created successfully"}, status=status.HTTP_201_CREATED)
            except:
                return Response(reg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(reg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    '''
    This view deals with the user information
        As of now it only returns username and id, but can be implemented to return any field such as email, address, ect..
        To add new fields, first they must be set up under the user model and then added here.
    '''

    # These routes are private to the user
    permission_classes = [PersonalInformationPrivacyPermission]

    def get(self, request, format=None):
        '''
        This function retrieves all defined user informations
        '''
        user = User.objects.get(pk=request.user.id)

        res_data = {
            "username": user.username,
            "user_id": user.id
        }

        return Response(res_data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        '''
        TODO
        This function retrieves and modifies allowed user informations
        '''
        user = User.objects.get(pk=request.user.id)

        res_data = {
            "username": user.username,
            "user_id": user.id
        }

        return Response(res_data, status=status.HTTP_200_OK)
