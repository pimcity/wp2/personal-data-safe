from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.db import models
from .models import User

class UserAdminConfig(UserAdmin):
    '''
    Custom Admin page config to better display information in the admin page
    '''

    model = User
    search_fields = ('username',)
    list_filter = ('username', 'is_active', 'is_superuser')
    ordering = ('-start_date',)
    list_display = ('id', 'username', 'is_active', 'is_superuser', 'start_date')
    fieldsets = (
        (None, {'fields': ('username', 'start_date',)}),
        ('Permissions', {'fields': ('is_superuser', 'is_active')}),
    )

admin.site.register(User, UserAdminConfig)