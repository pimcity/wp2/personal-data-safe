from django.urls import path
from .views import UserCreate, UserDetail, BlacklistTokenView

app_name = 'users'

urlpatterns = [
    path('register/', UserCreate.as_view(), name="user_registration"),
    path('logout/', BlacklistTokenView.as_view(), name="user_logout"),
    path('getAccountInfo/', UserDetail.as_view(), name="user_getInfo"),
]