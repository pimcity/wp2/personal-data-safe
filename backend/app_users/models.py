from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils import timezone


class UserManager(BaseUserManager):
    '''
    Custom User Managager class which allows to create a normal user and a superuser
    '''

    def create_superuser(self, username, password, **other_fields):
        '''
        This function allows the UserManager to set-up the is_superuser field of a user
        before createing a new superuser.
        '''

        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must be assigned to is_superuser=True')

        return self.create_user(username, password, **other_fields)

    def create_user(self, username, password, **other_fields):
        '''
        This function allows the UserManager to create a new user
        '''

        if not username:
            raise ValueError('Must provide an email for account creation.')

        user = self.model(username=username, **other_fields)
        user.set_password(password)
        user.save()
        return user

class User(AbstractUser):
    '''
    Custom User class that requires username and password.
    Also has some basic fields such as:
        is_staff (boolean): defines superusers
        is_active (boolean): defines active users
    '''
    username = models.CharField(max_length=40, unique=True)
    start_date = models.DateTimeField(default=timezone.now)
    is_active = models .BooleanField(default=True)

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.username