#!/bin/bash

find ../ -path ../venv -prune -false -o -type d -name 'migrations' -exec rm -rf {} \;
rm ../db.sqlite3