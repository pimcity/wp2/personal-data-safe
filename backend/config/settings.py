from pathlib import Path
from datetime import timedelta
import os


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r*rj83k4ne4$^=&($23h!6o@uh#4%!a08*s@=tcn&m31o8ky@a'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True


# OAUTH RESOURCE SERVER
# Introspection URL for EasyPIMS:
# https://easypims.pimcity-h2020.eu/identity/auth/realms/pimcity/protocol/openid-connect/token/introspect
OAUTH2_PROVIDER = {
    'RESOURCE_SERVER_INTROSPECTION_URL':          os.environ.get('OIDC_INTROSPECTION_URL'),
    'RESOURCE_SERVER_INTROSPECTION_CREDENTIALS': (os.environ.get('OIDC_CLIENT'),
                                                  os.environ.get('OIDC_SECRET')),
    'RESOURCE_SERVER_AUTH_TOKEN':                 os.environ.get('OIDC_TOKEN'),
    'OAUTH2_VALIDATOR_CLASS': 'app_personal_data.utils.custom_validator.CustomOAuth2Validator',
    'SCOPES': {
        'read:pds': 'Read scope',
        'create:pds': 'Write scope',
        'delete:pds': 'Delete scope',
        'update:pds': 'Update scope',
    },

}

TASK_MANAGER_ENDPOINT=os.environ.get('TASK_MANAGER_ENDPOINT') # The URL
TASK_MANAGER_APIKEY=os.environ.get('TASK_MANAGER_APIKEY') # The API Token
#A mapping like:
#personal-information:upload-personal-details-data,browsing-history:upload-browsing-data,location-history:upload-location-data
TASK_MANAGER_GROUP2TASK=os.environ.get('TASK_MANAGER_GROUP2TASK')


DATA_BUYER_OAUTH2_SCOPE={"databuyer:pds": "Data Buyer Scope"}


STATIC_URL = "/pds/api/static/"

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 3rd-party apps
    'rest_framework',
    'corsheaders',
    'rest_framework_simplejwt.token_blacklist',
    'oauth2_provider',

    # Local apps
    'app_personal_data',
    'app_users',
    'app_databuyers',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
]

ROOT_URLCONF = 'config.urls'

# Admin Login URL
LOGIN_URL = 'pds/api/admin/login/'
LOGOUT_REDIRECT_URL = '/login/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pds_postgres',
        'USER': 'admin',
        'PASSWORD': 'admin_secret_password',
        'HOST': 'localhost',
        'PORT': '',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True



# Rest Framework Settings

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

AUTHENTICATION_BACKENDS = (
    'oauth2_provider.backends.OAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)


# Cors Headers Settings
# TODO For production need to modify
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000",
    "http://192.168.1.122:8000"
]

ALLOWED_HOSTS = [
    "192.168.1.122",
    "192.168.24.130",
    "192.168.236.185",
    "127.0.0.1",
    "localhost",
    "0.0.0.0",
    "easypims.pimcity-h2020.eu",
]


# Custom user model
AUTH_USER_MODEL = 'app_users.User'


# Simple JWT settings

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(hours=4),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,
    'UPDATE_LAST_LOGIN': False,

    'ALGORITHM': 'HS256',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUDIENCE': None,
    'ISSUER': None,

    'AUTH_HEADER_TYPES': ('Bearer', 'JWT'),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}


# Setting path for the schema.yaml file
SCHEMA_PATH = 'schema.yaml'
