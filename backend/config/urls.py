from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from app_users.views import CustomTokenObtainPairView, CustomTokenRefreshView, TokenFromSession

urlpatterns = [

    url(r'^pds/api/', include([
        path('admin/', admin.site.urls),
        path('v1/', include('app_personal_data.urls')),
        path('users/', include('app_users.urls')),
        path('token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
        path('token-from-session/', TokenFromSession.as_view(), name='token-from-session'),
        path('token/refresh/', CustomTokenRefreshView.as_view(), name='token_refresh'),
        path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
        path('data-buyers/', include('app_databuyers.urls')),
        path("o/", include('oauth2_provider.urls', namespace='oauth2_provider')),
        #path('oidc/', include('mozilla_django_oidc.urls')),

    ])),

]
