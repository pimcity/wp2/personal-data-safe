import os
import django

# Next two lines are needed for imports, they need to be on top
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from app_databuyers.models import DatabuyerRequest
import sys

DatabuyerRequest.objects.all().delete()