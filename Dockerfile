# Build with:
# docker build . --tag martino90/personal-data-safe
# Push with:
# docker push martino90/personal-data-safe
# Play with:
# docker run -it --rm --entrypoint /bin/bash martino90/personal-data-safe
# Backend:  cd backend;  OIDC_RP_CLIENT_SECRET="key" python3 manage.py runserver 0.0.0.0:8000
# Frontend: cd frontend; npm start

FROM ubuntu:20.04
LABEL maintainer="Martino Trevisan"

RUN apt-get update
RUN apt-get install -y python3 python3-pip git libpq-dev python-dev curl

# Backend
RUN cd /opt ; git clone https://gitlab.com/pimcity/wp2/personal-data-safe
RUN pip3 install -r /opt/personal-data-safe/backend/requirements.txt

#Frontend
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install nodejs
RUN cd /opt/personal-data-safe/frontend/ ; npm install

WORKDIR /opt/personal-data-safe
ENTRYPOINT ["/bin/bash"]